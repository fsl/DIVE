# <img src="dive/static/dive_avatar.jpg" alt="DIVE" width="100" height="100"> DIVE - Diffusion models Interactive ViEwer


# Overview
DIVE is an interactive viewer of diffusion MRI models.

The viewer allows you to choose amongst various microstructural compartments, fibre ODFs, and acquisition parameters. Several interactive plots are displayed to allow visualisation of how different model parameters affect the diffusion data under different acquisition regimes.

# Installation
You can install DIVE on your computer using:

```buildoutcfg
git clone https://git.fmrib.ox.ac.uk/fsl/DIVE.git
cd DIVE
pip install .
```

Then to run DIVE:

```buildoutcfg
dive --show
```
This will bring up a web browser and you can begin playing with DIVE! 

# Compartment models
DIVE implements the following diffusion compartments:

- Tensor-like
  - Ball
  - Stick
  - Zeppelin
  - Tensor
  - Dot
- Restricted
  - Sphere
  - Cylinder

You can create any multi-compartment model using combinations of the above. You can also define a fibre orientation distribution on any subset of the compartments. 

# Pre-baked multicompartment models
In addition to choosing your own model, DIVE contains the following pre-baked multi-compartment models of diffusion that can serve as a starting point:

1. Ball \& Stick [(Behrens 2003)](https://pubmed.ncbi.nlm.nih.gov/14587019) 
2. NODDI: Neurite Orientation Dispersion and Density Imaging [(Zhang 2012)](https://pubmed.ncbi.nlm.nih.gov/22484410)
3. VERDICT: Vascular Extracellular and Restricted Diffusion for Cytometry in Tumours [(Panagiotaki 2015)](https://pubmed.ncbi.nlm.nih.gov/25426656) 
4. SANDI: Soma And Neurite Density Imaging ([Palombo 2020)](https://pubmed.ncbi.nlm.nih.gov/32289460)
5. MMWMD: Minimal Model for White Matter Diffusion [(Sepehrband 2016)](https://pubmed.ncbi.nlm.nih.gov/26748471)


# More information
Please check the [Project Wiki](https://git.fmrib.ox.ac.uk/fsl/DIVE/-/wikis/home) for the latest documentation.

# Demos
Check out this quick [demo](https://www.youtube.com/embed/9c2-Y1DtbVQ) for a tour of DIVE features (sound on!).
