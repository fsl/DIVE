from dive import acquisition
from dive.acquisition import load_presets
from dive.compartments.tensor import Tensor
from dive.compartments.stick import Stick
from dive.compartments.ball import Ball
from dive.compartments.cylinder import Cylinder
from dive import odf
import numpy as np


def test_convolve_watson():
    hcp_acq = load_presets()['HCP']

    # test ball (ODF shouldn't change the signal)
    ball = Ball(S0=1, diffusivity=3)
    sig = ball(**hcp_acq)
    rot_sig = odf.convolve_watson(ball, hcp_acq, 0.5, np.pi/2, np.pi/2, 10000)
    np.testing.assert_allclose(sig, rot_sig)

    # test stick (delta like ODF in the same direction shouldn't change signal)
    stick = Stick(S0=1, diffusivity=1)
    sig = stick(**hcp_acq)
    rot_sig = odf.convolve_watson(stick, hcp_acq, odi=1e-3, theta=0, phi=np.pi/2, integral_samples=10000)
    np.testing.assert_allclose(sig, rot_sig, atol=1e-3, rtol=1e-2)

    # arbitrary rotation shouldn't produce nans
    rot_sig = odf.convolve_watson(stick, hcp_acq, odi=1e-2, theta=np.pi/4, phi=np.pi/4, integral_samples=10000)
    assert not np.isnan(rot_sig).any()

    # test vectorized version
    rot_sig_1 = odf.convolve_watson(stick, hcp_acq, odi=1e-2, theta=np.pi/4, phi=np.pi/4,
                                    integral_samples=10000, vectorize=False)
    rot_sig_2 = odf.convolve_watson(stick, hcp_acq, odi=1e-2, theta=np.pi/4, phi=np.pi/4,
                                    integral_samples=10000, vectorize=True)
    np.testing.assert_allclose(rot_sig_2, rot_sig_1, atol=1e-7)


def test_coordinate_transforms():
    cart = np.array([1, 1, np.sqrt(2 / 3)])
    theta, phi, r = odf.cart2spherical(cart)
    np.testing.assert_allclose(theta, np.pi / 3)
    np.testing.assert_allclose(phi, np.pi / 4)
    np.testing.assert_allclose(np.squeeze(odf.spherical2cart(theta, phi, r)), cart, rtol=1e-3, atol=1e-6)


def analytic_stick():
    # to test integration gives the same results as analytic result for simple compartments.
    pass


def test_valid_convolve_delta():
    # check bvec is rotated so that fibre orientation is along z-axis
    dummy_acqusition = {"bval": np.ones(3),
                        "bvec": np.eye(3)}

    param = np.array([np.pi/2, np.pi/2])
    compartment = Stick()
    signal = odf.convolve_delta(compartment, dummy_acqusition, *param)
    stick = Stick()
    axial = stick(bval=1, bvec=np.array((0, 0, 1)))
    radial = stick(bval=1, bvec=np.array((0, 1, 0)))
    assert tuple(signal) == (radial, axial, radial)

    signal = odf.convolve_delta(compartment, dummy_acqusition, 0, 0)
    assert tuple(signal) == (radial, radial, axial)

    # check that ball signal is the same
    dummy_acqusition = {"bval": np.array([1]),
                        "bvec": np.array([0., 1., 0.])}

    param = np.array([np.pi/2, np.pi/2])
    compartment = Ball()
    signal = odf.convolve_delta(compartment, dummy_acqusition, *param)
    ball = Ball()
    assert signal == ball(bval=1)

    # check signal dimensions for multiple bvals and bvecs
    dummy_acqusition = {"bval": np.array([1., 2., 3., 4.]).T,
                        "bvec": np.array([[1., 0., 0.],
                                          [0., 1., 0.],
                                          [0., 0., 1.],
                                          [1 / 3, 1 / 3, 1 / 3]])}

    param = np.array([np.pi / 2, 0])

    signal = odf.convolve_delta(compartment, dummy_acqusition, *param)
    assert np.squeeze(signal).shape[0] == 4


def test_convolution_for_tensor():
    # test zeppelin first:
    zeppelin = Tensor(diffusivity_ax=1.7, diffusivity_perp1=1, diffusivity_perp2=1)
    acq = acquisition.load_presets()['HCP']
    signal = zeppelin(**acq)
    rot_signal = odf.convolve_crossing(zeppelin, acq, [(1, 'watson', (0, 0, 0))])
    np.testing.assert_allclose(signal, rot_signal, atol=1e-6)
    # test general tensor:
    tensor = Tensor(diffusivity_ax=1.7, diffusivity_perp1=1, diffusivity_perp2=1)
    acq = acquisition.load_presets()['HCP']
    signal = tensor(**acq)
    rot_signal = odf.convolve_crossing(tensor, acq, [(1, 'delta', (0, 0))])
    np.testing.assert_allclose(signal, rot_signal, atol=1e-6)

def test_convolution_cylinder():
    # Simply run to check that it doesn't throw an error
    try:
        cyl = Cylinder()
        acq = acquisition.load_presets()['HCP']
        odf.convolve_watson(cyl, acq, odi=.2, theta=np.pi / 4, phi=np.pi / 4,
                            integral_samples=1000, vectorize=True)
    except:
        raise


# def test_invalid_convolve_delta():
#
#     # check to make sure that tensors are not included
#
#     dummy_acqusition = {"bval": np.array([1]),
#                         "bvec": np.array([0., 1., 0.])}
#
#     param = np.array([np.pi/2, np.pi/2])
#     compartment = Tensor()
#
#     with pytest.raises(ValueError):
#         odf.convolve_delta(compartment, dummy_acqusition, *param)
