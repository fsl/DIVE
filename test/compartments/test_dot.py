from dive.compartments.dot import Dot
import pytest
import numpy as np

def test_valid_dots():
    dot = Dot(S0=1.3)
    assert dot.values['S0'] == 1.3

    default_dot = Dot()
    assert default_dot.values['S0'] == 1.

def test_invalid_dots():
    with pytest.raises(ValueError):
        Dot(unkown_parameter=20)