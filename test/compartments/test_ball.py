from dive.compartments.ball import Ball
import pytest
import numpy as np

def test_valid_balls():
    dot_ball = Ball(S0=1.3, diffusivity=0)
    assert dot_ball(bval=0.) == 1.3
    assert dot_ball(bval=1.) == 1.3
    assert dot_ball.values['S0'] == 1.3
    assert dot_ball.values['diffusivity'] == 0.

    fast_ball = Ball(S0=1.5, diffusivity=1e5)
    assert fast_ball(bval=0.) == 1.5
    assert fast_ball(bval=1.) < 1e-20
    assert fast_ball.values['S0'] == 1.5
    assert fast_ball.values['diffusivity'] == 1e5

    default_ball = Ball()
    assert default_ball.values['S0'] == 1.
    assert default_ball.values['diffusivity'] == 3.
    assert default_ball(bval=0.) == 1.
    assert default_ball(bval=1.) == np.exp(-3.)


def test_invalid_balls():
    with pytest.raises(ValueError):
        Ball(unkown_parameter=20)