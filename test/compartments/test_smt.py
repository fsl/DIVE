"""
Tests the spherical mean calculation
"""
from dive.compartments import Dot, Ball, Stick, Sphere, Cylinder, Tensor, Zeppelin
from dive.utils import fibonacci_sphere
from dive.acquisition import calc_G, load_presets
from numpy.testing import assert_allclose
import numpy as np


def test_compare_with_samples():
    bvec = fibonacci_sphere(1000)
    for compartment_class in (
        Dot, Ball, Stick, Sphere, Cylinder, Tensor, Zeppelin
    ):
        compartment = compartment_class()
        acquisition = {
            'bval': 2.,
            'diffusion_time': 10., # short diffusion time to be sensitive to sphere/cylinder radius
            'pulse_duration': 2.,
        }
        acquisition['G'] = calc_G(**acquisition)
        print(compartment)
        assert_allclose(
            compartment.spherical_mean(**acquisition),
            np.mean(compartment(bvec=bvec, **acquisition)),
            rtol=1e-2
        )

# This test was added because the smt now uses numpy.broadcast_shapes which was only introduced for numpy >=1.20.0
# which break dependency on python 3.6
def test_spherical_mean():
    stick = Stick()
    acq   = load_presets()['HCP']

    assert np.isclose(stick.spherical_mean(**acq)[3],0.3976894315708453)



