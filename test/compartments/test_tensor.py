from dive.compartments.tensor import Tensor
from dive.compartments.stick import Stick
from dive.compartments.ball import Ball
import pytest
import numpy as np


def test_valid_tensors():
    # No diffusion
    dot_tensor = Tensor(S0=1.3,
                        diffusivity_ax=0,
                        diffusivity_perp1=0,
                        diffusivity_perp2=0)
    z_direction = np.array([0., 0., 1.]).T  # (N,3)

    assert dot_tensor(bval=0., bvec=z_direction) == 1.3
    assert dot_tensor(bval=1., bvec=z_direction) == 1.3
    assert dot_tensor.values['S0'] == 1.3
    assert dot_tensor.values['diffusivity_ax'] == 0
    assert dot_tensor.values['diffusivity_perp1'] == 0
    assert dot_tensor.values['diffusivity_perp1'] == 0

    # Only axial diffusion
    stick_tensor = Tensor(S0=1.3,
                          diffusivity_ax=3.,
                          diffusivity_perp1=0,
                          diffusivity_perp2=0)
    stick = Stick(S0=1.3,
                  diffusivity=3)

    assert stick_tensor(bval=0., bvec=z_direction) == 1.3
    assert stick_tensor(bval=2., bvec=z_direction) == stick(bval=2., bvec=z_direction)

    # Isotropic diffusion
    ball_tensor = Tensor(S0=1.3,
                         diffusivity_ax=3.,
                         diffusivity_perp1=3,
                         diffusivity_perp2=3)
    ball = Ball(S0=1.3,
                diffusivity=3)
    assert ball_tensor(bval=0., bvec=z_direction) == 1.3
    assert ball_tensor(bval=2., bvec=z_direction) == ball(bval=2.)

    # Fast isotropic diffusion
    fast_ball_tensor = Tensor(S0=1.3,
                              diffusivity_ax=1e5,
                              diffusivity_perp1=1e5,
                              diffusivity_perp2=1e5)
    assert fast_ball_tensor(bval=0., bvec=z_direction) == 1.3
    assert fast_ball_tensor(bval=1., bvec=z_direction) < 1e-20

    # Default tensor
    default_tensor = Tensor()
    assert default_tensor.values['S0'] == 1.
    assert default_tensor.values['diffusivity_ax'] == 1.7
    assert default_tensor.values['diffusivity_perp1'] == 1.
    assert default_tensor.values['diffusivity_perp2'] == 0.5

    # Test dimensions of signal based on multiple bvecs
    mutliple_direction_tensor = Tensor()
    bvals = np.array([100., 300., 500., 500., ]).T
    bvecs = np.array([[1., 0., 0.],
                      [0., 1., 0.],
                      [0., 0., 1.],
                      [1 / 3, 1 / 3, 1 / 3]])

    assert np.squeeze(mutliple_direction_tensor(bval=bvals, bvec=bvecs)).shape[0] == 4


def test_invalid_tensor():
    with pytest.raises(ValueError):
        Tensor(unkown_parameter=20)
