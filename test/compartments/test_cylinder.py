from dive.compartments.stick import Stick
from dive.compartments.cylinder import Cylinder
import pytest
import numpy as np
from dive.acquisition import load_presets


def test_valid_cylinder():

    presets = load_presets()

    # Check that cylinder with zero radius is stick
    stick = Stick(diffusivity=1.7,S0=1.3)
    cylinder = Cylinder(radius=0.,diffusivity=1.7,S0=1.3)

    pred_stick    = stick(bval=presets["HCP"]["bval"], bvec=presets["HCP"]["bvec"])
    pred_cylinder = cylinder(bvec = presets["HCP"]["bvec"],
                             pulse_duration = presets["HCP"]["pulse_duration"],
                             diffusion_time = presets["HCP"]["diffusion_time"],
                             G = presets["HCP"]["G"])

    assert np.max(np.abs(pred_stick-pred_cylinder)) < 1e-10


def test_invalid_cylinder():
    with pytest.raises(ValueError):
        Cylinder(unkown_parameter=20)