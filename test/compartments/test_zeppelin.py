from dive.compartments.zeppelin import Zeppelin
from dive.compartments.stick import Stick
from dive.acquisition import load_presets
import pytest
import numpy as np

def test_valid_zeppelin():

    presets = load_presets()
    default_zeppelin = Zeppelin()
    hcp_bval, hcp_bvec = presets["HCP"]["bval"], presets["HCP"]["bvec"]
    # Check comp runs with default bvecs and bvals & produces correct output with correct size
    assert default_zeppelin(bval=hcp_bval, bvec=hcp_bvec).shape == presets["HCP"]["bval"].shape

    # No diffusion
    dot_zeppelin = Zeppelin(S0=1.3,
                        axial_diffusivity=0.,
                        radial_diffusivity=0)

    assert (dot_zeppelin(bval=hcp_bval, bvec=hcp_bvec) == 1.3).all()
    assert dot_zeppelin.values['S0'] == 1.3
    assert dot_zeppelin.values['axial_diffusivity'] == 0
    assert dot_zeppelin.values['radial_diffusivity'] == 0

    # Fast diffusion
    fast_zeppelin = Zeppelin(S0=1.3,
                             axial_diffusivity=3.,
                             radial_diffusivity=2.)
    # Check diffusion along zeppelin = S0 when b=0
    assert (fast_zeppelin(bval=0, bvec=np.eye(3)) == 1.3).all()
    assert (fast_zeppelin(bval=3, bvec=np.eye(3)) == [1.3 * np.exp(-2 * 3), 1.3 * np.exp(-2 * 3), 1.3 * np.exp(-3 * 3)]).all()

    # Test zeppelin converges to a stick for radial_diffusivity=0
    stick_zeppelin = Zeppelin(S0=1.3,
                          axial_diffusivity=3.,
                          radial_diffusivity=0)
    stick = Stick(S0=1.3,
                  diffusivity=3)

    assert (stick_zeppelin(bval=hcp_bval, bvec=hcp_bvec) == stick(bval=hcp_bval, bvec=hcp_bvec)).all()

    # Test if zeppelin converges to pancake for axial_diffusivity=0.

    # Test if zeppelin is centre-symmetric
    zep = Zeppelin(S0=1.3,axial_diffusivity=2.,radial_diffusivity=1.)
    bval = np.array([1.,1.])
    x,y,z = np.array([1.,1.,1.]) / np.linalg.norm(np.array([1.,1.,1.]))
    bvec = np.array([[x,y,z],[-x,-y,-z]])
    pred = zep(bval=bval,bvec=bvec)
    assert pred[0] == pred[1]
    # Test if zeppelin is axially symmetric
    x,y,z = np.array([1.,1.,0.]) / np.linalg.norm(np.array([1.,1.,0.]))
    bvec = np.array([[x,y,z],[-x,-y,-z]])
    pred = zep(bval=bval,bvec=bvec)
    assert pred[0] == pred[1]

    # Check default values
    default_zeppelin = Zeppelin()
    assert default_zeppelin.values['S0'] == 1
    assert default_zeppelin.values['axial_diffusivity'] == 1.7
    assert default_zeppelin.values['radial_diffusivity'] == 1.


def test_invalid_zeppelin():
    with pytest.raises(ValueError):
        Zeppelin(unkown_parameter=20)
