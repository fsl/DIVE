from dive.compartments.stick import Stick
import pytest
import numpy as np
from dive.acquisition import load_presets

def test_valid_stick():

    presets = load_presets()

    default_stick = Stick()
    # Check comp runs with default bvecs and bvals & produces correct outpout with correct size
    assert default_stick(bval=presets["HCP"]["bval"], bvec=presets["HCP"]["bvec"]).shape == presets["HCP"]["bval"].shape
    # Check default values
    assert default_stick.values['S0'] == 1
    assert default_stick.values['diffusivity'] == 1.7


    fast_stick = Stick(S0=1.3, diffusivity=3)
    # Check diffusion along stick = S0 when b=0
    assert fast_stick(bval=0, bvec=np.array((0, 0, 1), ndmin=2)) == 1.3
    assert fast_stick(bval=3, bvec=np.array((0, 0, 1), ndmin=2)) == 1.3*np.exp(-3 * 3)
    # Check diffusion perp to stick
    assert fast_stick(bval=3, bvec=np.array((0, 1, 0), ndmin=2)) == 1.3
    # Check default values
    assert fast_stick.values['S0'] == 1.3
    assert fast_stick.values['diffusivity'] == 3

    slow_stick = Stick(S0=1.7, diffusivity=0)
    # Check diffusion along stick = S0 when b=0
    assert slow_stick(bval=0, bvec=np.array((0, 0, 1), ndmin=2)) == 1.7
    assert slow_stick(bval=3, bvec=np.array((0, 0, 1), ndmin=2)) == 1.7
    # Check diffusion perp to stick
    assert slow_stick(bval=3, bvec=np.array((0, 1, 0), ndmin=2)) == 1.7
    # Check default values
    assert slow_stick.values['S0'] == 1.7
    assert slow_stick.values['diffusivity'] == 0

def test_invalid_stick():
    with pytest.raises(ValueError):
        Stick(unkown_parameter=20)
