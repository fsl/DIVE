from dive.compartments import Sphere
from dive.compartments.sphere import gamma_ms
import numpy as np
import pytest


def test_valid_spheres():
    diffusivity = 1.7
    acq_params = dict(G=30, diffusion_time=80, pulse_duration=40)

    #  test for r=0 , i.e. dot
    dot_sphere = Sphere(S0=1, diffusivity=diffusivity, radius=1e-3)
    np.testing.assert_allclose(dot_sphere(**acq_params), 1.)

    # test inside variables has not changed by accident:
    assert dot_sphere.values['S0'] == 1.
    assert dot_sphere.values['diffusivity'] == diffusivity
    assert dot_sphere.values['radius'] == 1e-3

    # test for r -> inf , i.e. unrestricted diffusion:
    infinite_sphere = Sphere(S0=1, diffusivity=diffusivity, radius=1e3)
    b = gamma_ms ** 2 * (acq_params['G'] * 1e-9) ** 2 * \
        acq_params['pulse_duration'] ** 2 * (acq_params['diffusion_time'] - acq_params['pulse_duration'] / 3)
    np.testing.assert_allclose(infinite_sphere(**acq_params), np.exp(-b * diffusivity), rtol=1, atol=1e-6)


def test_invalid_sphere():
    with pytest.raises(ValueError):
        Sphere(unkown_parameter=20)
