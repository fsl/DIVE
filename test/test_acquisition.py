import numpy as np
import os.path as op
from dive.acquisition import load_presets

_ROOT = op.abspath(op.dirname(__file__))
_ROOT = op.join(op.dirname('_ROOT'), 'dive')
HCP_BVECS = np.genfromtxt(op.join(_ROOT, "data/HCP/bvecs"))
HCP_BVALS = np.genfromtxt(op.join(_ROOT, "data/HCP/bvals")) / 1000
UKBB_BVECS = np.genfromtxt(op.join(_ROOT, "data/Biobank/bvecs"))
UKBB_BVALS = np.genfromtxt(op.join(_ROOT, "data/Biobank/bvals")) / 1000


def test_acquisition():
    presets = load_presets()
    assert np.array_equal(presets["HCP"]["bval"], HCP_BVALS)
    assert np.array_equal(presets["HCP"]["bvec"], HCP_BVECS.T)
    assert np.array_equal(presets["Biobank"]["bval"], UKBB_BVALS)
    assert np.array_equal(presets["Biobank"]["bvec"], UKBB_BVECS.T)
    if "HCP" in presets:
        assert presets['HCP']['bval'].shape[0] == 288, 'Length of bvals wrong'
        assert presets['HCP']['G'].shape[0] == 288, 'Length of G wrong'
        assert presets['HCP']['pulse_duration'].shape[0] == 288, 'Length of pulse_duration wrong'
        assert presets['HCP']['diffusion_time'].shape[0] == 288, 'Length of diffusion_time wrong'
    if "Biobank" in presets:
        assert presets['Biobank']['bval'].shape[0] == 105, 'Length of bvals wrong'
        assert presets['Biobank']['G'].shape[0] == 105, 'Length of G wrong'
        assert presets['Biobank']['pulse_duration'].shape[0] == 105, 'Length of pulse_duration wrong'
        assert presets['Biobank']['diffusion_time'].shape[0] == 105, 'Length of diffusion_time wrong'
    for acq_choice in presets.keys():
        assert presets[acq_choice]['bvec'].shape[1] == 3, 'Dimension of bvecs is not 3'
        #assert np.any(presets[acq_choice]['bval'] > 500), 'Check if unit of bvals is ms/micron^2'


from dive.acquisition import calc_G
def test_calc_G():
    assert np.isclose(calc_G(3,10,50),94.7765694741686)
