from dive.utils import get_data
import os.path

def test_get_data():
    assert os.path.exists(get_data("HCP", "bvecs"))
    assert os.path.exists(get_data("HCP", "bvals"))
    assert os.path.exists(get_data("Biobank", "bvecs"))
    assert os.path.exists(get_data("Biobank", "bvals"))

from dive.utils import fibonacci_sphere
import numpy as np
def test_fibonacci_sphere():
    s = fibonacci_sphere(50)
    assert s.shape == (50,3), "Output shape should be (N,3)"
    assert np.isclose(s.mean(),0.000187)
    assert np.isclose(s.std(),0.57735)

from dive.utils import calc_FA_MD
def test_calc_FA_MD():
    FA,MD = calc_FA_MD(np.array([[2.19092173, 3.30998015, 1.06217652, 5.26909453, 1.40035463,0.71482436]]))
    assert np.isclose(FA,0.97657847)
    assert np.isclose(MD,2.72494687)


