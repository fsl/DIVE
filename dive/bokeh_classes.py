"""
This contains classes to create and read out the Bokeh interface for global settings.

These global settings include:
- Acquisition parameters
- Compartments of the model

The main outside interface is with the `Model` class, which contains the user-selected settings 
and a `get_signal` function to compute the signal given those settings.

The actual settings are split across various `Selector` classes, which create and read out a group of settings.
"""
from bokeh.models.widgets.buttons import Dropdown
from dive import compartments, acquisition, odf, utils
from bokeh.layouts import row, column
from bokeh.models import Select, Slider, CheckboxButtonGroup, Spinner
from bokeh.models import FileInput
from bokeh.models import Button, CustomJS, ColumnDataSource
from bokeh.models import Toggle
import base64
from typing import List, Sequence
from bokeh.models import PreText
import numpy as np
from contextlib import contextmanager
from os.path import dirname, join
import panel as pn

UPDATES_DISABLED = False
DEFAULT_MARGIN = 10 # provides some spacing around UI elements in cards

@contextmanager
def disable_plot_updates():
    """
    Temporarily disable updating the plot when the sliders get updated

    This allows updating of several slider values (e.g., during fitting) without the plots being regenerated after each slider update.

    Use like:
    ```python
    with disable_plot_updates():
        ... # adjust sliders
    model.update()
    ```
    """
    global UPDATES_DISABLED
    old_value = UPDATES_DISABLED
    UPDATES_DISABLED = True
    yield
    UPDATES_DISABLED = old_value



class Model:
    """
    Represents all the user-provided settings

    Attributes for model evaluation:

        - `compartments`: list of model compartments
        - `acquisition`: dictionary of acquisition parameters

    Attributes for bokeh interface:

        - `top_tabs`: Bokeh tabs to put at the top of the webpage with the model compartment selection
        - `side_tabs`: Bokeh tabs to put at the left of the webpage with the other settings

    Use the `get_signal` method to compute the model signal.
    """
    def __init__(self, ):
        self.callbacks = []

        with disable_plot_updates():
            self.model_selector = MultiCompartmentSelector(self)
            self.odf_selector = CrossingFibresSelector(self)
            self.acquisition_selector = AcquisitionSelector(self)
            self.shell_selector = ShellSelector(self)
            self.noise_selector = NoiseSelector(self)
            self.parameter_selector = ParameterSelector(self, self.model_selector)
            self.axis_modifier = AxisModifier(self)

        self.update()

        self.side_tabs = pn.Tabs(
            self.acquisition_selector.bokeh_object,
            self.shell_selector.bokeh_object,
            self.axis_modifier.bokeh_object,
            active=1
        )

        self.top_tabs = pn.Tabs(
            self.model_selector.bokeh_object, 
            self.odf_selector.bokeh_object,
            dynamic=True,
        )

    @property
    def compartments(self, ):
        return self.model_selector.compartments

    @property
    def acquisition(self, ):
        return self.acquisition_selector.acquisition

    @property
    def odf_params(self, ):
        return self.odf_selector.odf_params

    @property
    def use_odf(self, ):
        return self.odf_selector.use_odf

    @property
    def shell(self, ):
        return self.shell_selector.shell

    @property
    def parameter(self, ):
        return self.parameter_selector.parameter

    @property
    def orientation(self, ):
        return self.axis_modifier.orientation

    def update(self, ):
        """Updates all the plots.

        This should be called whenever anything in the model changes.
        """
        if not UPDATES_DISABLED:
            self.acquisition_selector.get_scatter()
            for func in self.callbacks:
                func(None, None, None)

    def register_callback(self, callback_function):
        """
        Registers a callback function to be called if any part of the model changes
        """
        self.callbacks.append(callback_function)

    def get_signal(self, acquisition=None, override_microstructure=(), add_noise=False):
        """
        Get signal for the supplied acquisition parameters

        Without any arguments the user-selected acquisition parameter set will be used.
        If any acquisition parameters are provided, they will override the user-selected ones.

        Args:
            acquisition: acquisition parameters (defaults to user-selected ones)
            override_microstructure: list of (index, name, value) triplets. Will override the `name` parameter of the `index` comparment with `value`.
            add_noise: if True add noise to the signal
        """
        acquisition = self.acquisition if acquisition is None else acquisition
        signal = np.zeros(acquisition['bvec'].shape[0])
        
        for index_c, (c, use_odf) in enumerate(zip(self.compartments, self.odf_selector.use_odf)):
            acq_compartment = dict(acquisition)
            for idx, name, value in override_microstructure:
                if idx == index_c:
                    acq_compartment[name] = value
            if use_odf:
                new_signal = odf.convolve_crossing(c, acq_compartment, self.odf_params)
            else:
                new_signal = c(**acq_compartment)
            signal = signal + new_signal

        if add_noise:
            signal = utils.add_noise(signal,
                                     self.noise_selector.noise_dropdown.value,
                                     SNR=self.noise_selector.noise_slider.value,
                                     S0=self.get_spherical_mean(bval=0))

        return signal

    def override_acquisition(self, calculate, overriden):
        """
        Returns user-selected shell with optionally some parameters overriden

        Args:
            calculate: 'G' or 'bval'; defines which parameter should be recomputed after overriding the acquisition parameters
            overriden: dictionary with parameters to override the user-provided shell parameters
        """
        if calculate in overriden:
            raise ValueError(f"Can not both calculate {calculate} and override it")
        if calculate not in ('G', 'bval'):
            raise ValueError(f"I don't know how to calculate {calculate}")
        shell = self.shell
        func = {
            'G': acquisition.calc_G,
            'bval': acquisition.calc_bval,
        }[calculate]
        for param, value in overriden.items():
            shell[param] = value

        keywords = {
            key: value for key, value in shell.items() 
            if key in acquisition.update_on_change and key != calculate
        }
        shell[calculate] = func(**keywords)
        return shell

    def get_shell_signal(self, calculate='G', override_microstructure=(), **override_acquisition):
        """
        Computes the signal for the user-selected shell with optionally some parameters overriden

        Args:
            calculate: 'G' or 'bval'; defines which parameter should be recomputed after overriding the acquisition parameters
            override_acquisition: allows overriding the user-provided shell parameters
        """
        return self.get_signal(self.override_acquisition(calculate, override_acquisition), override_microstructure, add_noise=False)

    def get_spherical_mean(self, calculate='G', override_microstructure=(), **override_acquisition):
        """
        Computes the spherical mean signal for the user-selected shell with optionally some parameter overriden

        Args:
            calculate: 'G' or 'bval'; defines which parameter should be recomputed after overriding the acquisition parameters
            override_acquisition: allows overriding the user-provided shell parameters
        """
        shell = self.override_acquisition(calculate, override_acquisition)
        signal = 0
        for index_c, c in enumerate(self.compartments):
            acq_compartment = dict(shell)
            for idx, name, value in override_microstructure:
                if idx == index_c:
                    acq_compartment[name] = value
            signal = signal + c.spherical_mean(**acq_compartment)
        return signal


class MultiCompartmentSelector:
    """
    Creates and reads out the bokeh interface for a multi-compartment model

    references for preset models:
    
        1. Behrens TEJ, Woolrich MW, Jenkinson M, et al. Characterization and propagation of uncertainty in diffusion-weighted MR imaging. Magn Reson Med. 2003;50(5):1077-1088. doi:10.1002/mrm.10609
        2. Zhang H, Schneider T, Wheeler-Kingshott CA, Alexander DC. NODDI: Practical in vivo neurite orientation dispersion and density imaging of the human brain. NeuroImage. 2012;61(4):1000-1016. doi:10.1016/j.neuroimage.2012.03.072
        3. Panagiotaki E, Walker-Samuel S, Siow B, et al. Noninvasive Quantification of Solid Tumor Microstructure Using VERDICT MRI. Cancer Res. 2014;74(7):1902-1912. doi:10.1158/0008-5472.CAN-13-2511
        4. Palombo M, Ianus A, Guerreri M, et al. SANDI: A compartment-based model for non-invasive apparent soma and neurite imaging by diffusion MRI. NeuroImage. 2020;215:116835. doi:10.1016/j.neuroimage.2020.116835
        5. Sepehrband F, Alexander DC, Kurniawan ND, Reutens DC, Yang Z. Towards higher sensitivity and stability of axon diameter estimation with diffusion‐weighted MRI. NMR Biomed. 2016;29(3):293-308. doi:10.1002/nbm.3462
    """
    preset_models = {
        'Ball & Stick': [compartments.Ball(diffusivity=1.7), compartments.Stick(diffusivity=1.7)],
        'NODDI': [compartments.Ball(diffusivity=3),
                  compartments.Stick(diffusivity=1.7),
                  compartments.Zeppelin(axial_diffusivity=1.7, radial_diffusivity=1.7/2)],
        'VERDICT': [compartments.Sphere(diffusivity=0.9, radius=5.),
                    compartments.Ball(diffusivity=0.9),
                    compartments.Stick(diffusivity=3.1)],
        'SANDI': [compartments.Stick(diffusivity=1.7),
                  compartments.Sphere(diffusivity=3, radius=5.),
                  compartments.Zeppelin(axial_diffusivity=1.7, radial_diffusivity=1.7/2)],
        'MMWMD': [compartments.Dot(),
                            compartments.Ball(diffusivity=3),
                            compartments.Zeppelin(axial_diffusivity=0.6, radial_diffusivity=.6/2),
                            compartments.Cylinder(diffusivity=0.6)],
    }


    def __init__(self, parent: Model):
        """
        Creates the interface to define a multi-compartment model
        """
        self.compartment_checkboxes: List[CompartmentCheckBoxGroup] = []
        self.ncompartment_selector = Spinner(title='number of compartments', value=1, low=1, high=10, width_policy='min')
        self.ncompartment_selector.on_change('value', self.update_ncompartments)
        self.preset_model_selector = Dropdown(label="select predefined model", menu=list(self.preset_models.keys()), width_policy='min')
        self.preset_model_selector.on_click(self.preset_model_click)
        self.selectors: List[CompartmentSelector] = []
        self.bokeh_row = row()
        self.parent = parent
        self.update_ncompartments(None, 0, 1)
        self.bokeh_object = pn.Column(
            pn.Row(self.ncompartment_selector, self.preset_model_selector),
            self.bokeh_row, 
            margin=DEFAULT_MARGIN, 
            name='Model compartments')

    def update_ncompartments(self, attr, old, new):
        """
        Updates the compartments after number of compartments is changed
        """
        assert old == len(self.selectors)
        with disable_plot_updates():
            while new > len(self.selectors):
                self.selectors.append(CompartmentSelector(self))
                self.bokeh_row.children.append(self.selectors[-1].bokeh_object)
            while new < len(self.selectors):
                self.selectors.pop()
                self.bokeh_row.children.pop()
            for c in self.compartment_checkboxes:
                c.update()
        self.update()

    def update(self, ):
        """
        Generic update function for if anything in the multi-compartment object changes
        """
        self.parent.update()

    def preset_model_click(self, attr):
        self.compartments = self.preset_models[attr.item]

    @property
    def compartments(self, ):
        return [c.compartment for c in self.selectors]

    @compartments.setter
    def compartments(self, new_compartments):
        with disable_plot_updates():
            self.ncompartment_selector.value = len(new_compartments)
            for selector, compartment in zip(self.selectors, new_compartments):
                selector.compartment = compartment
        self.update()

class CompartmentSelector:
    """
    Provides and reads out the bokeh interface for a single compartment
    """
    def __init__(self, parent: MultiCompartmentSelector):
        self.parent = parent
        all_compartments = compartments.list_compartments()
        self.selector = Select(options=[c.name for c in all_compartments], value=all_compartments[0].name)
        self.selector.on_change('value', self.update_compartment_type)
        self.index = len(self.parent.selectors)
        self.bokeh_object = column(self.selector, column())
        self.update_compartment_type(None, None, None)

    @property
    def compartment(self, ):
        values = [slider.value for slider in self.sliders]
        cls = self.get_compartment_class()
        return cls(**{param.name: value for param, value in zip(cls.parameters, values)})

    @compartment.setter
    def compartment(self, new_compartment: compartments.BaseCompartment):
        with disable_plot_updates():
            self.selector.value = new_compartment.name
            for slider, parameter in zip(self.sliders, new_compartment.parameters):
                slider.value = new_compartment.values[parameter.name]
        self.parent.update()

    def get_compartment_class(self, ) -> compartments.BaseCompartment:
        """
        Helper function to get the compartment class based on the user-selected compartment name
        """
        model_name = self.selector.value
        for model_cls in compartments.list_compartments():
            if model_cls.name == model_name:
                return model_cls
        raise ValueError("invalid compartment selection")

    def update_compartment_values(self, attr, old, new):
        """This callback is called whenever anything in the model changes.
        """
        self.parent.update()

    def update_compartment_type(self, attr, old, new):
        """This callback is called whenever the compartment type changes or a new compartment is added.
        """
        self.sliders = [p.bokeh_slider() for p in self.get_compartment_class().parameters]
        for slider in self.sliders:
            slider.on_change('value_throttled', self.update_compartment_values)
        self.bokeh_object.children[-1] = column(*self.sliders)
        with disable_plot_updates():
            for c in self.parent.compartment_checkboxes:
                c.update()
        self.update_compartment_values(None, None, None)

class CompartmentCheckBoxGroup:
    """Creates and keeps up to date a series of checkboxes to select one or more compartments.
    """
    def __init__(self, compartment_selector: MultiCompartmentSelector, default_selected=False, **kwargs):
        self.bokeh_object = CheckboxButtonGroup(**kwargs)
        self.compartment_selector = compartment_selector
        self.default_selected = default_selected
        self.compartment_selector.compartment_checkboxes.append(self)
        self.update()

    def on_change(self, func):
        return self.bokeh_object.on_change('active', func)

    def update(self, ):
        """
        Called if the number of compartments or the compartment names change
        """
        nold = len(self.bokeh_object.labels)
        self.bokeh_object.labels = [c.name for c in self.compartment_selector.compartments]
        if self.default_selected:
            self.bokeh_object.active = self.bokeh_object.active + list(range(nold, len(self.bokeh_object.labels)))

    @property
    def indices(self, ):
        return self.bokeh_object.active

    @property
    def mask(self, ):
        """Returns True for each selected compartment.
        """
        return [idx in self.indices for idx in range(len(self.compartment_selector.compartments))]


class ParameterSelector:
    """
    Selects a single parameter from a single compartment.

    Provides the user with two drop-down menus. The first allows to select a compartment. The second allows to select a parameter
    """
    def __init__(self, parent, compartment_selector: MultiCompartmentSelector):
        """
        Creates a new set of dropboxes to select a parameter

        Args:
            parent: `parent.update` will be called whenever the selection changes
            compartment_selector: the interface to select the compartments
        """
        self.compartment_selector = compartment_selector
        self.compartment_selector.compartment_checkboxes.append(self)
        self.parent = parent
        options = [f'{select.index + 1}. {select.selector.value}' for select in compartment_selector.selectors]
        self.compartment_menu = Select(options=options, value=options[0])
        self.compartment_menu.on_change('value', self.update_compartment)
        options = self.compartment_selector.selectors[self.compartment_index].compartment.parameter_names
        self.parameter_menu = Select(options=options, value=options[0])
        self.parameter_menu.on_change('value', self.update_parameter)
        self.bokeh_object = pn.Row(
            pn.Column(
                PreText(text="select compartment:", styles={'color': 'grey'}),
                self.compartment_menu,
            ),
            pn.Column(
                PreText(text="select parameter:", styles={'color': 'grey'}),
                self.parameter_menu,
            ),
        )

    @property
    def compartment_index(self, ):
        return list(self.compartment_menu.options).index(self.compartment_menu.value)

    @property
    def parameter(self, ):
        idx = self.compartment_index
        c = self.compartment_selector.selectors[idx].compartment
        if self.parameter_menu.value not in c.parameter_names:
            with disable_plot_updates():
                self.update_compartment(None, None, None)
        idx_param = c.parameter_names.index(self.parameter_menu.value)
        return (idx, self.parameter_menu.value, c.parameters[idx_param])

    def update_compartment(self, attr, old, new):
        """
        Called when the user selects a different compartment
        """
        compartment = self.compartment_selector.selectors[self.compartment_index].compartment
        keep_value = self.parameter_menu.value in compartment.parameter_names
        self.parameter_menu.options = compartment.parameter_names
        if not keep_value:
            self.parameter_menu.value = compartment.parameter_names[0]
        else:
            self.update_parameter(None, None, None)

    def update_parameter(self, attr, old, new):
        """
        Called whenever the user changes their selected parameter
        """
        self.parent.update()

    def update(self, ):
        """
        Called whenever the user changes the number of type of compartments
        """
        options = [f'{select.index + 1}. {select.selector.value}' for select in self.compartment_selector.selectors]
        keep_compartment = self.compartment_menu.value in options
        old_index = self.compartment_index
        self.compartment_menu.options = options
        if not keep_compartment:
            if old_index >= len(options):
                old_index = len(options) - 1
            self.compartment_menu.value = options[old_index]


class NoiseSelector:
    # deals with noise
    # TODO: class description
    def __init__(self, parent: Model):
        self.parent = parent
        self.noise_types = utils.noise_types
        # selection dropdown
        self.noise_dropdown = Select(options=list(self.noise_types), value='none', title='select noise type:')
        self.noise_dropdown.on_change('value', self.noise_dropdown_callback)
        # noise level slider
        self.noise_slider = Slider(title='change SNR', value=30, start=1, end=100)
        self.noise_slider.on_change('value_throttled', self.noise_dropdown_callback)
        self.noise_slider.disabled = True
        # TODO: how to change slider text colour
        self.noise_bokeh_object = pn.Column(
            self.noise_dropdown, self.noise_slider, 
            margin=DEFAULT_MARGIN, name='Noise'
        )

    def noise_dropdown_callback(self, attr, old, new):
        self.noise_slider.disabled = False
        self.parent.update()


class AcquisitionSelector:
    """
    Provides and reads out the bokeh interface for the acquisition parameters
    """
    def __init__(self, parent: Model):
        self.parent = parent
        self.preset = acquisition.load_presets()
        self.acquisition_selector = Select(options=list(self.preset.keys()) +
                                                   ['User defined (b-values)', 'User defined (gradient strength)'],
                                           value='HCP', title='select acquisition parameters:')
        self.acquisition_selector.on_change('value', self.update_acquisition)
        self.acquisition = self.preset[self.acquisition_selector.value]

        # toggle to choose whether to display the data
        self.toggle = Toggle(label='Display scatter', active=False)
        self.toggle.on_click(self.toggle_callback)

        # button for saving the scatter data
        self.scatter_data = ColumnDataSource(data=dict())
        self.save_button = Button(label="Download scatter", button_type="success",disabled=True)
        self.save_button.js_on_click(CustomJS(args=dict(source=self.scatter_data),
                                              code=open(join(dirname(__file__), "download.js")).read()))

        # for user defined files
        # Add file dialog button here
        # for bvals
        self.txtbvals = PreText(text="bvals (ms/um^2):", styles={'color': 'black'})
        self.file_input_bvals = FileInput()
        # for bvecs
        self.txtbvecs = PreText(text="bvecs:", styles={'color': 'black'})
        self.file_input_bvecs = FileInput()

        # for g
        self.txtG = PreText(text="gradient strength (mT/m):", styles={'color': 'black'})
        self.file_input_G = FileInput()

        # for diffusion time
        self.txtdiffusiontime = PreText(text="diffusion time (ms):", styles={'color': 'black'})
        self.file_input_diffusiontime = FileInput()

        # for pulse duration
        self.txtpulseduration = PreText(text="pulse duration (ms):", styles={'color': 'black'})
        self.file_input_pulseduration = FileInput()

        # apply button
        self.file_apply_button = Button(label='Apply user-defined acquisition')
        self.file_apply_button.on_click(self.apply_btn_callback)

        self.bval_col = [self.txtbvals,
                                self.file_input_bvals,
                                self.txtbvecs,
                                self.file_input_bvecs,
                                self.file_apply_button]

        self.G_col = [self.txtG,
                       self.file_input_G,
                       self.txtdiffusiontime,
                       self.file_input_diffusiontime,
                       self.txtpulseduration,
                       self.file_input_pulseduration,
                       self.txtbvecs,
                       self.file_input_bvecs,
                       self.file_apply_button]

        self.user_selection = column()
    
        self.bokeh_object = pn.Column(
                self.acquisition_selector,
                self.user_selection,
                self.toggle,
                self.save_button,
                margin=DEFAULT_MARGIN,
                name='Acquisition'
        )

        self.update_acquisition(None, None, None)

    def update_acquisition(self, attr, old, new):

        if 'User' not in self.acquisition_selector.value:
            self.acquisition = self.preset[self.acquisition_selector.value]
            self.user_selection.children = []

        elif 'b-value' in self.acquisition_selector.value:
            self.user_selection.children = self.bval_col

        elif 'gradient' in self.acquisition_selector.value:
            self.user_selection.children = self.G_col

        else:
            raise ValueError('Selector is not defined.')

        self.parent.update()

    def apply_btn_callback(self):
        # decode the content of the file search dialog before passing to function
        if 'b-values' in self.acquisition_selector.value:
            self.user_acq = acquisition.load_user_input(bvals=base64.b64decode(self.file_input_bvals.value).decode(),
                                                        bvecs=base64.b64decode(self.file_input_bvecs.value).decode())
        else:
            self.user_acq = acquisition.load_user_input(G=base64.b64decode(self.file_input_G.value).decode(),
                                                        bvecs=base64.b64decode(self.file_input_bvecs.value).decode(),
                                                        diffusion_time=base64.b64decode(self.file_input_diffusiontime.value).decode(),
                                                        pulse_duration=base64.b64decode(self.file_input_pulseduration.value).decode())
        self.acquisition = self.user_acq
        self.parent.update()

    def toggle_callback(self,attr):
        # update display of the scatter points
        self.save_button.disabled = not self.toggle.active

        if self.toggle.active:
            self.parent.side_tabs.append(self.parent.noise_selector.noise_bokeh_object)
        else:
            self.parent.side_tabs.remove(self.parent.noise_selector.noise_bokeh_object)

        self.parent.update()

    def get_scatter(self):
        #self.update_acquisition(None,None,None)


        signal = self.parent.get_signal(self.acquisition,add_noise=True)
        self.scatter_data.data = {'signal'  : signal,
                       'bvals (ms/um^2)'   : self.acquisition['bval'],
                       'bvecs-x' : self.acquisition['bvec'][:,0],
                       'bvecs-y' : self.acquisition['bvec'][:,1],
                       'bvecs-z' : self.acquisition['bvec'][:,2],
                       'G (mT/m)' : self.acquisition['G'],
                       'pulse duration (ms)' : self.acquisition['pulse_duration'],
                       'diffusion time (ms)' : self.acquisition['diffusion_time']}


class CrossingFibresSelector:
    """
    Provides and reads out the bokeh interface for one or more crossing fibres
    """
    _signal_fraction_fix_in_progress = False

    def __init__(self, parent: Model):
        self.parent = parent

        self.ncrossing_slider = Spinner(title='number of crossing fibres', value=1, low=1, high=10, width=100)
        self.ncrossing_slider.on_change('value', self.update_ncompartments)

        self.odf_param_row = row()
        self.selectors: List[SingleFibreSelector] = []

        self.compartment_choice = CompartmentCheckBoxGroup(self.parent.model_selector, default_selected=True)
        self.select_compartment_row = pn.Column(
            pn.pane.HTML("apply ODF to: ", styles={'font-size': '100%'}),
            self.compartment_choice.bokeh_object,
            
        )
        # self.select_compartment_row.children[1].on_change('active', self.update_odf_selection)
        self.compartment_choice.bokeh_object.on_change('active', self.update_odf_selection)

        self.bokeh_object = pn.Column(
            self.select_compartment_row, self.ncrossing_slider, 
            self.odf_param_row, margin=DEFAULT_MARGIN,
            name='Fibre distribution',
        )

        self.update_ncompartments(None, 0, 1)

    def update_ncompartments(self, attr, old, new):
        """
        Updates the crossign fibres after number of crossing fibres is changed
        """
        assert old == len(self.selectors)
        while new > len(self.selectors):
            self.selectors.append(SingleFibreSelector(self))
            self.odf_param_row.children.append(self.selectors[-1].bokeh_object)
            self.selectors[-1].sliders[0].value = 1 / len(self.selectors)
            self.fix_signal_fractions([len(self.selectors) - 1])
        while new < len(self.selectors):
            self.selectors.pop()
            self.odf_param_row.children.pop()
        self.fix_signal_fractions()
        
        self.selectors[0].sliders[0].disabled = new == 1

        self.parent.update()

    def update_odf_selection(self, attr, old, new):
        """
        Updates when the selection for compartments in the ODF changes
        """
        self.parent.update()

    def fix_signal_fractions(self, fixed=()):
        """
        Ensure the signal fractions add up to 1

        Args:
            fixed: sequence of integers indicating which signal fractions should not be adjusted
        """
        if self._signal_fraction_fix_in_progress:
            return
        adjustable = [selector for idx, selector in enumerate(self.selectors) if idx not in fixed]
        remainder = 1 - sum([selector.signal_fraction for idx, selector in enumerate(self.selectors) if idx in fixed])
        if remainder < 0:
            raise ValueError("Failed to have signal fractions add up to 1 as the fixed signal fractions already exceed 1")
        elif remainder == 0:
            # when one fibre orientation goes to 1, the others should not go to exactly zero as this will cause the relative ratio between them to be forgotten
            remainder = 1e-20
        total_adjustable = sum([selector.signal_fraction for selector in adjustable])
        self._signal_fraction_fix_in_progress = True
        if total_adjustable != 0:
            ratio = remainder / total_adjustable
            for selector in adjustable:
                selector.sliders[0].value = ratio * selector.sliders[0].value
        else:
            for selector in adjustable:
                selector.sliders[0].value = remainder / len(adjustable)
        self._signal_fraction_fix_in_progress = False

    @property
    def odf_params(self, ):
        """List of ODF parameters

        Format as expected by the `odf.convolve_crossing` function.
        """
        return [selector.odf_params for selector in self.selectors]

    @property
    def use_odf(self, ):
        return self.compartment_choice.mask


class SingleFibreSelector:
    """
    Provides and reads out the bokeh interface for one crossing fibre
    """
    def __init__(self, parent: CrossingFibresSelector):
        self.parent = parent

        self.sliders = (
            Slider(title='signal fraction', value=0, start=0, end=1, step=0.01),
            Slider(title='theta', value=0, start=0, end=90),
            Slider(title='phi', value=0, start=0, end=180),
            Slider(title='dispersion', value=0, start=0, end=1, step=0.01),
        )

        self.sliders[0].on_change('value_throttled', self.update_signal_fraction)
        for slider in self.sliders[1:]:
            slider.on_change('value_throttled', self.update)

        self.bokeh_object = column(*self.sliders)

    @property
    def signal_fraction(self, ) -> float:
        """Signal fraction for this fibre population.
        """
        return self.sliders[0].value

    @property
    def theta(self, ) -> float:
        """Angle in radians.
        """
        return self.sliders[1].value / 180 * np.pi

    @property
    def phi(self, ) -> float:
        """Angle in radians
        """
        return self.sliders[2].value / 180 * np.pi

    @property
    def dispersion(self, ) -> float:
        """Orientation dispersion index
        """
        return self.sliders[3].value

    @property
    def odf_params(self, ):
        """ODF parameters as expected by the `odf.convolve_crossing` function.
        """
        if self.dispersion == 0.:
            return (self.signal_fraction, 'delta', (self.theta, self.phi))
        else:
            return (self.signal_fraction, 'watson', (self.dispersion, self.theta, self.phi))

    def update(self, attr, old, new):
        return self.parent.parent.update()

    def update_signal_fraction(self, attr, old, new):
        if not self.parent._signal_fraction_fix_in_progress:
            self.parent.fix_signal_fractions([self.parent.selectors.index(self)])
            self.update(attr, old, new)


class ShellSelector:
    """
    Creates and reads out a user-provided shell
    """
    _in_update = False

    def __init__(self, parent: Model):
        self.parent = parent

        self.sliders = {
            'bval': Slider(title="b-value (ms/um^2)", value=1, start=0, end=20, step=0.1),
            'diffusion_time': Slider(title="diffusion time (ms)", value=50, start=10, end=200, step=1),
            'pulse_duration': Slider(title="pulse duration (ms)", value=15, start=1, end=30, step=1),
            'G': Slider(title="gradient strength (mT/m)", value=100, start=0, end=1200, step=10),
        }

        self._in_update = True
        self.update_gradient_strength(None, None, None)
        self._in_update = False

        self.bokeh_object = pn.Column(
            *list(self.sliders.values()),
            margin=DEFAULT_MARGIN,
            name='Shells'
        )

        update_funcs = {
            'G': self.update_gradient_strength,
            'bval': self.update_bval,
        }
        for changed, to_change in acquisition.update_on_change.items():
            self.sliders[changed].on_change(
                'value_throttled', update_funcs[to_change]
            )

    @property
    def shell(self, ):
        res = {key: slider.value for key, slider in self.sliders.items()}
        return res

    def update_gradient_strength(self, attr, old, new):
        """Slider got updated which means the gradient strength needs recomputing
        """
        if self._in_update:
            return
        self._in_update = True
        self.sliders['G'].value = acquisition.calc_G(**{
            key: value for key, value in self.shell.items() 
            if key in acquisition.update_on_change and key != 'G'
        })
        self._in_update = False
        self.parent.update()

    def update_bval(self, attr, old, new):
        """Slider got updated which means the gradient strength needs recomputing
        """
        if self._in_update:
            return
        self._in_update = True
        self.sliders['bval'].value = acquisition.calc_bval(**{
            key: value for key, value in self.shell.items() 
            if key in acquisition.update_on_change and key != 'bval'
        })
        self._in_update = False
        self.parent.update()

class AxisModifier:
    """
    Applies user-selected transformations to the signal, b-value, or diffusion time

    Supported are:
    - no transform (linear)
    - log-transform
    - inversion (1/x)
    - 1/sqrt(x)
    """
    # changes axis using mathematical transformations
    # TODO: class description
    xforms_map = {
        'linear': lambda x: x,
        'log(x)': lambda x: np.log10(x),
        '1/x': lambda x: 1 / x,
        '1/sqrt(x)': lambda x: 1 / np.sqrt(x)
    }
    def __init__(self, parent: Model):
        self.parent = parent
        self.xforms = ['linear', 'log(x)', '1/x', '1/sqrt(x)']
        self.axes_to_transform = ['signal', 'bval', 'diffusion_time']
        # selection dropdown
        self.axis_dropdown = Select(options=list(self.axes_to_transform), value='signal', title='select axis:')
        self.axis_dropdown.on_change('value', self.axis_dropdown_callback)
        self.xform_dropdown = Select(options=list(self.xforms), value='linear', title='select transform:')
        self.xform_dropdown.on_change('value', self.xform_dropdown_callback)

        self.orientation = {
            'smt': Toggle(label='spherical mean', active=True),
            'theta': Slider(title='direction (theta)', value=0., start=0., end=90., step=1, disabled=False),
            'phi': Slider(title='direction (phi)', value=0., start=0., end=180, step=.1, disabled=False)
        }
        self.orientation['smt'].on_click(self.smt_update)
        self.orientation['theta'].on_change('value_throttled',self.orientation_update)
        self.orientation['phi'].on_change('value_throttled',self.orientation_update)

        self.bokeh_object = pn.Column(
            self.axis_dropdown, self.xform_dropdown, 
            *self.orientation.values(), margin=DEFAULT_MARGIN,
            name='Axis',
        )
        # xforms
        self.selected_xforms = {name: 'linear' for name in self.axes_to_transform}

    def axis_dropdown_callback(self, attr, old, new):
        if self.axis_dropdown.value != 'none':
            self.xform_dropdown.disabled = False
        else:
            self.xform_dropdown.disabled = True
        with disable_plot_updates():
            self.xform_dropdown.value = self.selected_xforms[self.axis_dropdown.value]

    def xform_dropdown_callback(self, attr, old, new):
        self.selected_xforms[self.axis_dropdown.value] = self.xform_dropdown.value
        self.parent.update()

    def orientation_update(self, attr, old, new):
        self.parent.update()

    def smt_update(self, attr):
        self.parent.update()

    @property
    def xforms_to_apply(self, ):
        """
        Mapping of signal/bval/diffusion_time to the transformation function
        """
        return {axis: self.xforms_map[value] for axis, value in self.selected_xforms.items()}

    @property
    def xforms_to_apply_name(self, ):
        """
        Mapping of signal/bval/diffusion_time to a string that should be prepended to the axis label to describe the transform
        """
        return {axis: "" if value == 'linear' else f" [{value}]" for axis, value in self.selected_xforms.items()}
