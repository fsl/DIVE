from panel.theme.material import DarkTheme
from panel.theme.material import MaterialDefaultTheme
from dive import plotting
from dive.bokeh_classes import Model
import panel as pn

#  Get page template

# theme = MaterialDarkTheme
theme = MaterialDefaultTheme
template = pn.template.MaterialTemplate(title='FSL DIVE', theme=theme)


# Override some CSS for font size and card title alignment

css = '''
.bk-root {
    font-family: Helvetica, Arial, sans-serif;
    font-size: 100%;
}
.bk.bk-clearfix {
    display: inline-block;
    width: 100%;
    font-size: 80%;
    text-align: left;
}
'''
pn.config.raw_css.append(css)


# instantiate model
model = Model()

plotting_funcs = [plotting.plot_signal_vs_acquisition, plotting.plot_fa_md_vs_acquisition]

plot_tabs = pn.Tabs(*[
    (title, pn.Row(*plots, max_height=500)) for title, plots in [
        ('Sensitivity [b-value]', [f(model, 'bval') for f in plotting_funcs]),
        ('Sensitivity [diff time]', [f(model, 'diffusion_time') for f in plotting_funcs]),
        ('Orientation', [plotting.plot_signal_vs_bvec(model), plotting.plot_odf_glyph(model)]),
]])

plot_tabs.append((
    'Sensitivity [params]',
    pn.Column(
        pn.Row(*[f(model, 'microstructure') for f in plotting_funcs], max_height=600),
        model.parameter_selector.bokeh_object),
))

# add content to template

template.sidebar.append(model.side_tabs)
template.sidebar_width = 380

template.header_background = '#002147'
template.logo = 'dive/static/dive_avatar.jpg'
template.site_url = 'https://git.fmrib.ox.ac.uk/fsl/DIVE'

template.main.extend(
    [model.top_tabs, plot_tabs]
)

template.servable()
