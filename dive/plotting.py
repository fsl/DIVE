"""Contains functions useful for plotting

Generic plotting function will take a `bokeh_classes.Model` as input to indicate which multi-compartment model to plot. 
This model will contain a `get_signal` function to compute the signal for the user-selected acquisition parameters (which can be overriden).
The output should be a Bokeh object (plot or column/row with plot and sliders) that will be added to the main plotting grid.

Within the definition of the plotting function you will probably want to:
- define one or more ColumnDataSources containing the actual data
- *optionally* create one or more sliders
- write a function which fills the ColumnDataSources with data given the slider values and using the model `get_signal` function. 
  This function should accept three parameters called "attr", "old", and "new", which should be ignored!
- add this function as a callback to the created sliders, so it gets called when the slider updates (`slider.on_change('value', <func>)`)
- add this function as a callback to the model, so it will be called on model update (`model.register_callback(<func>)`)
- create a bokeh figure and create the plot using the data in the DataSources
- return either just the plot or the plot and the sliders in a `column` object
"""
from bokeh.plotting import figure
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Slider, Range1d, LinearAxis
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import numpy as np
from dive import utils
from dive.bokeh_classes import Model
from .acquisition import calc_G, GAMMABAR, CONSTANTS

'''
Example plotting functions

- signal or log-signal vs bvalue
- signal or log-signal vs squared dot product of bvecs and V1
- signal or log-signal as a function of theta/phi on a 2D plot
- 3D glyph of signal or log-signal
- Radon transform of signal or log-signal vs theta/phi or Glyph
- Slices through 2D planes

'''

DEFAULT_PLOT_OPTS = {'tools': "pan,box_zoom,reset,save,hover",
                     'toolbar_location': "right", "sizing_mode": "scale_both", "aspect_ratio": 1.3}


acquisition_parameters = {
    'bval': (np.linspace(0, 10, 50), 'b-value (ms/um^2)', 'G'),
    'diffusion_time': (np.arange(3, 101, 1), 'diffusion time (ms)', 'G')
}


def plot_signal_vs_acquisition(model: Model, x='bval'):

    """
    Plot signal against b-value or diffusion time.

    Args:
       data : Dict of ColumnDataSources
       x : which acquisition parameter to plot on the x-axis ('bval' or 'diffusion_time')

    Returns:
        Bokeh column
    """

    # creating data sources
    data_points = ColumnDataSource()
    signal_profile = ColumnDataSource()

    change_acquisition = x != 'microstructure'
    if change_acquisition:
        xvalues, xlabel, calculate = acquisition_parameters[x]
    else:
        xlabel = 'test'


    def compute_source(attr, old, new):
        # signal/bval transforms
        f_y = model.axis_modifier.xforms_to_apply['signal']
        if x in ('bval', 'diffusion_time'):
            f_x = model.axis_modifier.xforms_to_apply[x]
        else:
            f_x = model.axis_modifier.xforms_map['linear']

        if model.acquisition_selector.toggle.active and change_acquisition:
            data_points.data.update(
                xval=f_x(model.acquisition[x]),
                signal=f_y(model.get_signal(add_noise=True)),
            )
            #utils.dpoint_add_noise(model, data_points)
        else:
            data_points.data.update(
                xval=[],
                signal=[],
            )

        # 1 diffusion time produces 1 b-value
        if change_acquisition:
            new_xvalues = xvalues
            if model.orientation['smt'].active:
                sig = np.asarray(model.get_spherical_mean(calculate, **{x: xvalues}))
            else:
                th,ph = [model.orientation[a].value for a in ['theta','phi']]
                n     = len(new_xvalues)
                bvec  = np.outer(np.ones(n),utils.sph2cart(th,ph,'degrees'))
                sig   = model.get_shell_signal(calculate, **{x: xvalues},bvec=bvec)

            if x in ('bval', 'diffusion_time'):
                fig.xaxis.axis_label = f'{xlabel} {model.axis_modifier.xforms_to_apply_name[x]}'
            else:
                fig.xaxis.axis_label = xlabel
        else:
            idx_compartment, name_parameter, parameter = model.parameter
            new_xvalues = np.linspace(parameter.min_value, parameter.max_value, 100)
            if model.orientation['smt'].active:
                sig = np.asarray(model.get_spherical_mean(override_microstructure=[(idx_compartment, name_parameter, new_xvalues)]))
            else:
                th,ph = [model.orientation[a].value for a in ['theta','phi']]
                n     = len(new_xvalues)
                bvec  = np.outer(np.ones(n),utils.sph2cart(th,ph,'degrees'))
                sig = np.asarray(model.get_shell_signal(bvec=bvec, override_microstructure=[(idx_compartment, name_parameter, new_xvalues)]))

            fig.xaxis.axis_label = f'{parameter.title} ({model.parameter_selector.compartment_menu.value})'
        if sig.ndim == 0:
            sig = np.array([sig] * new_xvalues.size)
        signal_profile.data.update(
            xval=f_x(new_xvalues),
            signal=f_y(sig),
        )
        fig.yaxis.axis_label = f'signal {model.axis_modifier.xforms_to_apply_name["signal"]}'

    # initial fill of the data source
    fig = figure(**DEFAULT_PLOT_OPTS)
    compute_source(None, None, None)

    # make sure the data sources get updated if the model changes
    model.register_callback(compute_source)

    # creating the actual plot based on the data in the data sources
    fig.circle(x='xval',
               y='signal',
               source=data_points,
               line_color='black')
    fig.line(x='xval',
             y='signal',
             source=signal_profile,
             line_color='red',
             line_width=3)
    return fig


def plot_signal_vs_bvec(model: Model):
    """
    Plot signal against b-vector

    Args:
       data : Dict of ColumnDataSources

    Returns:
        Bokeh column
    """

    # prepare data sources
    data_points = ColumnDataSource()
    signal_profile = ColumnDataSource()

    bvecs = utils.fibonacci_sphere(100)


    def compute_source(attr, old, new):
        f_y = model.axis_modifier.xforms_to_apply['signal']

        # sort according to dot product with chosen reference direction
        th, ph = [model.orientation[a].value for a in ['theta', 'phi']]
        ref_orientation = utils.sph2cart(th, ph, 'degrees')
        dp2 = np.abs(np.dot(bvecs, ref_orientation)) ** 2
        sbvecs = bvecs[dp2.argsort(), :]

        if model.acquisition_selector.toggle.active:
            data_points.data.update(
                dp2=np.abs(np.dot(model.acquisition['bvec'], ref_orientation))**2,
                signal=f_y(model.get_signal(add_noise=True)),
            )
            #utils.dpoint_add_noise(model, data_points)
        else:
            data_points.data.update(
                dp2=[],
                signal=[],
            )

        signal_profile.data.update(
            dp2=np.abs(np.dot(sbvecs, ref_orientation)) ** 2,
            signal=f_y(model.get_shell_signal(bvec=sbvecs)),
        )
        fig.yaxis.axis_label = f'signal {model.axis_modifier.xforms_to_apply_name["signal"]}'

    fig = figure(**DEFAULT_PLOT_OPTS)
    compute_source(None, None, None)

    model.register_callback(compute_source)


    fig.circle(x='dp2',
               y='signal',
               source=data_points,
               line_color='black')

    fig.xaxis.axis_label = 'cos(angle)^2'

    fig.line(x='dp2',
             y='signal',
             source=signal_profile,
             line_color='red',
             line_width=3)
    return fig


# PLOT FA/MD VS BVALUE
def plot_fa_md_vs_acquisition(model: Model, x='bval'):
    """
    Plot FA/MD against b-value

    Args:
       model object

    Returns:
        Bokeh figure
    """
    # creating data sources
    signal_profile = ColumnDataSource()

    change_acquisition = x != 'microstructure'
    if change_acquisition:
        xvalues, xlabel, calculate = acquisition_parameters[x]
    else:
        xlabel = 'test'
    bvecs = np.concatenate(([[0,0,0]],utils.fibonacci_sphere(samples=100)))
    invmat = np.linalg.pinv(utils.form_DTImat(1,bvecs))

    def compute_source(attr, old, new):
        # axis transform
        if x in ('bval', 'diffusion_time'):
            f_x = model.axis_modifier.xforms_to_apply[x]
        else:
            f_x = model.axis_modifier.xforms_map['linear']

        # recalculate signal
        if change_acquisition:
            sigb0 = np.atleast_1d(model.get_spherical_mean(bval=0))
            new_xvalues = f_x(xvalues)
            sig = np.array([
                np.concatenate((sigb0, model.get_shell_signal(calculate, bvec=bvecs[1:], **{x: xv})))
                for xv in xvalues]).T
            if x in ('bval', 'diffusion_time'):
                fig.xaxis.axis_label = f'{xlabel} {model.axis_modifier.xforms_to_apply_name[x]}'
            else:
                fig.xaxis.axis_label = xlabel
        else:
            idx_compartment, name_parameter, parameter = model.parameter
            new_xvalues = f_x(np.linspace(parameter.min_value, parameter.max_value, 100))
            sigb0 = np.array(model.get_spherical_mean(override_microstructure=[(idx_compartment, name_parameter, new_xvalues)], bval=0))
            sig = np.concatenate((sigb0[np.newaxis, :], np.array([model.get_shell_signal(
                    override_microstructure=[(idx_compartment, name_parameter, xv)], bvec=bvecs[1:])
                for xv in new_xvalues]).T), 0)
            
            fig.xaxis.axis_label = f'{parameter.title} ({model.parameter_selector.compartment_menu.value})'
        logsig = np.log( np.clip(sig,1e-20,None))
        log_components = np.dot(invmat, logsig)[1:]
        if change_acquisition:
            log_components /= model.override_acquisition(calculate, {x: xvalues})['bval']
        else:
            log_components /= model.shell['bval']
        FA, MD = utils.calc_FA_MD(log_components.T)
        signal_profile.data.update(
            xval=new_xvalues,
            FA=np.array(FA),
            MD=np.array(MD)
        )

    # initial fill of the data source
    fig = figure(**DEFAULT_PLOT_OPTS)
    fig.xaxis.axis_label = xlabel
    fig.yaxis.axis_label = 'MD'
    compute_source(None, None, None)

    # make sure the data sources get updated if the model changes
    model.register_callback(compute_source)

    # creating the actual plot based on the data in the data sources
    fig.extra_y_ranges = {"FA": Range1d(start=0, end=1)}
    fig.add_layout(LinearAxis(y_range_name="FA", axis_label='FA'), 'right')

    fig.line(x='xval',
             y='FA',
             source=signal_profile,
             line_color='black',
             line_width=3,y_range_name='FA',legend_label='FA')
    fig.line(x='xval',
            y='MD',
            source=signal_profile,
            line_color='red',
            line_width=3,legend_label='MD')
    fig.legend.location = "bottom_right"
    return fig


def plot_odf_glyph(model: Model):
    """
    Plot signal ODF glpyh

    Args:
       model object

    Returns:
        Plotly figure
    """

    th, ph = np.mgrid[0:2*np.pi:100j, 0:2*np.pi:100j]

    bvecs = utils.sph2cart(th, ph)
    bvecs_shape = bvecs.shape

    fig = go.Figure()
    fig.update_layout(
        scene=dict(
            aspectratio=dict(x=1, y=1, z=1),
            xaxis=dict(
                backgroundcolor="white",
                gridcolor="rgb(220, 220, 220)",
                showbackground=True,
                zerolinecolor="rgb(220, 220, 220)",
            ),
            yaxis=dict(
                backgroundcolor="white",
                gridcolor="rgb(220, 220, 220)",
                showbackground=True,
                zerolinecolor="rgb(220, 220, 220)"
            ),
            zaxis=dict(
                backgroundcolor="white",
                gridcolor="rgb(220, 220, 220)",
                showbackground=True,
                zerolinecolor="rgb(220, 220, 220)",
            ),
        ),
        margin=dict(
            r=0, l=0,
            b=0, t=0)
    )

    def compute_source(attr, old, new):
        sig = model.get_shell_signal(bvec=bvecs.reshape(-1, 3)).reshape(bvecs_shape[:2])
        X, Y, Z = bvecs[:, :, 0], bvecs[:, :, 1], bvecs[:, :, 2]

        # TODO: identify optimal way to update plot in plotly
        fig.data = []
        fig.add_trace(go.Surface(x=sig*X, y=sig*Y, z=sig*Z, showscale=False))

        fig.update_layout(scene=
            {dim + 'axis': dict(range=(-np.amax(sig), np.amax(sig))) for dim in 'xyz'}
        )

    compute_source(None, None, None)
    model.register_callback(compute_source)

    return fig
