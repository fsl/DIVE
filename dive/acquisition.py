from .utils import get_data
from typing import Dict
import numpy as np
import io

# CONSTANTS
GAMMABAR  = 42.577130 # Hz/G

# Below diffusion_time and pulse_duration were found in:
# HCP : Sotiropoulos et al. Neuroimage 2013
# UKBB : https://biobank.ndph.ox.ac.uk/showcase/showcase/docs/brain_mri.pdf
CONSTANTS = { 'HCP': {'pulse_duration':10.6,'diffusion_time':43.1}, 
              'Biobank' : {'pulse_duration':21.4,'diffusion_time':45.5} }


update_on_change = {
    'diffusion_time': 'G',
    'bval': 'G',
    'G': 'bval',
    'pulse_duration': 'bval',
}


def calc_G(bval, pulse_duration, diffusion_time, t_ramp=0):
    """
    Calculate G given diffusion times and bvalue
    bval must be in ms/mu^2
    pulse_duration/diffusion_time muust be in ms

    Returns:
        numpy array or float (Unit mT/m)
    """
    pulse_duration_s = pulse_duration / 1000  # ms --> s
    Delta_s = diffusion_time / 1000  # ms --> s
    t_ramp_s = t_ramp / 1000  # ms --> s

    del_term = (pulse_duration_s ** 2. * (Delta_s - pulse_duration_s / 3) + t_ramp_s ** 3 / 30 - pulse_duration_s * (t_ramp_s ** 2) / 6)
    return np.sqrt(bval * 1e3 / (4 * np.pi ** 2 * GAMMABAR ** 2) / del_term)


def calc_bval(G, pulse_duration, diffusion_time, t_ramp=0):
    """
    Calculate B given diffusion times and G
    pulse_duration/diffusion_time must be in ms

    Args:
        G: gradient strength in mT/m
        pulse_duration: pulse duration in ms
        diffusion_time: diffusion time in ms
        t_ramp: ramp time in ms

    Returns:
        numpy array or float (Unit ms/mu^2)
    """
    pulse_duration_s = pulse_duration / 1000  # ms --> s
    Delta_s = diffusion_time / 1000  # ms --> s
    t_ramp_s = t_ramp / 1000  # ms --> s
    del_term = (pulse_duration_s ** 2. * (Delta_s - pulse_duration_s / 3) + t_ramp_s ** 3 / 30 - pulse_duration_s * (
            t_ramp_s ** 2) / 6)
    return G ** 2 * (4 * np.pi ** 2 * GAMMABAR ** 2) * del_term / 1e3


def load_presets() -> Dict[str, Dict[str, np.ndarray]]:
    """
    Load acquisition parameters from previous MRI studies

    Returns:
        Mapping of study names to acquisition parameters
    """
    ret = {}
    for scheme in ['HCP', 'Biobank']:
        ret[scheme] = {'bval': np.genfromtxt(get_data(scheme, "bvals")) / 1000,
                       "bvec": np.genfromtxt(get_data(scheme, "bvecs")).T}
        if (
            scheme in CONSTANTS
            and 'pulse_duration' in CONSTANTS[scheme]
            and 'diffusion_time' in CONSTANTS[scheme]
        ):
            ret[scheme]['pulse_duration'] = CONSTANTS[scheme]['pulse_duration'] * np.ones_like(ret[scheme]['bval'])
            ret[scheme]['diffusion_time'] = CONSTANTS[scheme]['diffusion_time'] * np.ones_like(ret[scheme]['bval'])
            ret[scheme]['G'] = calc_G(
                bval=ret[scheme]['bval'],
                pulse_duration=CONSTANTS[scheme]['pulse_duration'],
                diffusion_time=CONSTANTS[scheme]['diffusion_time']
            )

    # Diffusion time experiments
    # AxCaliber from https://pubmed.ncbi.nlm.nih.gov/18506799/
    ax_pulse_duration = 4.   # ms
    ax_Gmax           = 300. # ms
    ax_diffusion_time = np.array([12.,30.,60.,100.,150.]) # ms
    ax_bvec           = np.array([1.,0.,0.]) # perp to main axon orientation
    ax_G              = np.linspace(0.,ax_Gmax,16)
    n_diff_times      = len(ax_diffusion_time)
    n_gradients       = len(ax_G)
    N                 = n_diff_times*n_gradients
    ret['AxCaliber'] = {
        'bvec'           : np.outer(np.ones(N),ax_bvec),
        'pulse_duration' : np.ones(N)*ax_pulse_duration,
        'diffusion_time' : np.outer(ax_diffusion_time,np.ones(n_gradients)).flatten(),
        'G'              : np.outer(np.ones(n_diff_times),ax_G).flatten(),
    }
    ret['AxCaliber']['bval'] = calc_bval(ret['AxCaliber']['G'],
                                         ret['AxCaliber']['pulse_duration'],
                                         ret['AxCaliber']['diffusion_time'])
    return ret


def load_user_input(bvals='', bvecs='', G='', diffusion_time='', pulse_duration='') -> Dict[str, Dict[str, np.ndarray]]:
    """

    Args:
        bvals: string of format n x 1
        bvecs: string of format n x 3
        diffusion_time: string of either 1 or n x 1 scalars
        pulse_duration: string of either 1 or n x 1 scalars
        G: string of format n x 1
        Only one of the G or b-vals must be provided.
    Returns:

    """
    assert len(bvecs) > 0, 'bvecs are required to load acquisition.'
    assert np.logical_xor(len(bvals),
                          np.all([len(G), len(diffusion_time), len(pulse_duration)])), \
        'either bval or all G, DT, PD must be provided.'
    # convert bvecs from bytes to np.array
    bvecs = [row.split() for row in bvecs.split('\n') if len(row.strip()) > 0]
    bvecs = np.array([np.array([float(el) for el in row]) for row in bvecs])
    if bvecs.shape[0] == 3:
        bvecs = bvecs.T
    elif bvecs.shape[1] == 3:
        pass
    else:
        AssertionError('bvec file has to be n x 3 or 3 x n.')

    n_acq = bvecs.shape[0]

    if len(bvals) == 0:
        G = G.rstrip().split()
        G = np.array([float(el) for el in G])

        diffusion_time = diffusion_time.rstrip().split()
        diffusion_time = np.array([float(el) for el in diffusion_time])

        pulse_duration = pulse_duration.rstrip().split()
        pulse_duration = np.array([float(el) for el in pulse_duration])

        bvals = calc_bval(G, pulse_duration, diffusion_time)

    elif len(G) == 0:
        bvals = bvals.rstrip().split()
        bvals = np.array([float(el) for el in bvals])
        bvals = bvals / 1000  # change the units
        diffusion_time = CONSTANTS['Biobank']['diffusion_time'] * np.ones(n_acq)
        pulse_duration = CONSTANTS['Biobank']['pulse_duration'] * np.ones(n_acq)

        G = calc_G(bvals, pulse_duration, diffusion_time)

    assert diffusion_time.shape[0] == n_acq, 'Diffusion time must have the same number of rows as bvecs'
    assert pulse_duration.shape[0] == n_acq, 'Pulse duration must have the same number of rows as bvecs'
    assert G.shape[0] == n_acq , 'Gradient strength must have the same number of rows as bvecs'
    assert bvals.shape[0] == n_acq, 'bvals must have the same number of rows as bvecs'

    assert np.all(pulse_duration < diffusion_time), 'Pulse duration has to be shorter than diffusion time.'

    # compute gradient based on other parameters.

    return {
        "bval": bvals,
        "bvec": bvecs,
        'G'   : G,
        'diffusion_time': diffusion_time,
        'pulse_duration': pulse_duration
    }
