import os.path as op
import numpy as np
from scipy.stats import rice
from functools import lru_cache

_ROOT = op.abspath(op.dirname(__file__))


def get_data(*args):
    #TODO: make this smarter so that we can pip install this project
    return op.join(_ROOT, "data", *args)


# Isotropically distributed points on unit sphere
@lru_cache(None)
def fibonacci_sphere(samples=1):
    """
    Creates N points uniformly-ish distributed on the sphere

    Args:
        samples : int
    """
    points = np.array((samples,3))
    phi = np.pi * (3. - np.sqrt(5.))  # golden angle in radians

    i = np.arange(samples)
    y = 1 - 2.*(i / float(samples-1))
    r = np.sqrt(1-y*y)
    t = phi * i
    x = np.cos(t) * r
    z = np.sin(t) * r

    points = np.asarray([x,y,z]).T

    return points


noise_types = ['none', 'gaussian', 'rician']


def add_noise(data, noise_type='gaussian', SNR=1, S0=1):
    """Add noise to signal

    For Gaussian :
            Snoise = S0 + N(0,sigma)
    For Rician :
            Snoise = sqrt( Real^2 + Imag^2  )
            where Real = S0+N(0,sigma) and Imag=N(0,sigma)
    In both case SNR is defined as SNR = S0/sigma

    Args:
        data : array-like
        noise_type : str
                     accepted values are 'gaussian', 'rician', or 'none'
        SNR : float
        S0  : float
    """
    if noise_type not in ['none','gaussian','rician']:
        raise(Exception(f'Unknown noiise type {noise_type}'))
    if noise_type == 'none':
        return data+0.
    else:
        sigma = S0 / SNR
        if noise_type == 'gaussian':
            noise = np.random.normal(loc=0.0, scale=sigma, size=data.shape)
            return data + noise
        elif noise_type == 'rician':
            noise_r = np.random.normal(loc=0.0, scale=sigma, size=data.shape)
            noise_i = np.random.normal(loc=0.0, scale=sigma, size=data.shape)
            return np.sqrt( (data+noise_r)**2 + noise_i**2 )


# THIS FUNCTION SHOULD BE DROPPED
# NOISE SHOULD BE ADDED WHEN CALLING MODEL.GET_SIGNAL()
#def dpoint_add_noise(model, data_points):
#    s0 = model.get_shell_signal(bvec=np.array([[0, 0, 1]]), bval=0)
#    data_points.data.update(
#        signal=add_noise(model.get_signal(),
#                   noise_type=model.noise_selector.noise_dropdown.value,
#                   SNR=model.noise_selector.noise_slider.value,
#                   S0=s0)
#s    )

#### Analytic Expression for FA and MD

def calc_FA(L1, L2, L3):
    """
    Calculate FA from Eigenvalues

    Args:
        L1,2,3 : float or array
    Returns:
        float or array
    """
    MD = (L1 + L2 + L3) / 3.
    return np.sqrt(3. / 2.) * np.sqrt(
        ((L1 - MD) ** 2 + (L2 - MD) ** 2 + (L3 - MD) ** 2) / (L1 ** 2 + L2 ** 2 + L3 ** 2 + 1e-100))


def calc_FA_MD(T):
    """
    Calculate FA and MD from tensor T using analytic formulas (i.e. no eigendecomposition)

    Args:
        T : Nx6 array
    Returns:
        (FA, MD)
    """
    # MD is simply the trace/3
    Tr = T[:, 0] + T[:, 3] + T[:, 5]
    MD = Tr / 3.

    # FA is more complicated....
    # Ref: https://arxiv.org/pdf/physics/0610206.pdf

    c2 = -Tr
    c1 = T[:, 0] * T[:, 3] + T[:, 0] * T[:, 5] + T[:, 3] * T[:, 5] - (T[:, 1] ** 2 + T[:, 2] ** 2 + T[:, 4] ** 2)
    c0 = T[:, 0] * T[:, 4] ** 2 + T[:, 3] * T[:, 2] ** 2 + \
         T[:, 5] * T[:, 1] ** 2 - T[:, 0] * T[:, 3] * T[:,5] - 2. * T[:,1] * T[:,2] * T[:,4]
    p = c2 ** 2 - 3. * c1
    q = -27. / 2 * c0 - c2 * c2 * c2 + 9. / 2. * c2 * c1
    insqrt = np.clip(27.*(c1 ** 2 * (p - c1) / 4. + c0 * (q + 27. / 4. * c0)),0,None)
    nom = np.sqrt(insqrt)
    phi = 1 / 3. * np.arctan(nom / (q + 1e-100))
    cosphi = np.cos(phi)
    sinphi = np.sin(phi)
    x1 = 2. * cosphi
    x2 = -cosphi - np.sqrt(3.) * sinphi
    x3 = -cosphi + np.sqrt(3.) * sinphi
    sqrtp = np.sqrt(np.clip(p, 0, None))
    L1 = sqrtp / 3. * x1 - c2 / 3.
    L2 = sqrtp / 3. * x2 - c2 / 3.
    L3 = sqrtp / 3. * x3 - c2 / 3.
    FA = calc_FA(L1, L2, L3)

    FA[MD <= 1e-8] = np.nan
    return FA, MD

def form_DTImat(bval,bvec):
    """
    Create matrix for running GLM to fit DTI model
    Args:
        bval : (N,) array
        bvec : (N,3) array

    Returns:
        (N,7) array
    """
    if not np.isscalar(bval):
        assert len(bval) == bvec.shape[0], f'len(bval)={len(bval)} but bvec.shape[0]={bvec.shape[0]}'
    assert bvec.shape[1] == 3

    return np.array([np.ones_like(bvec[:,0]),
     -bval * bvec[:, 0] * bvec[:, 0],
     -bval * bvec[:, 0] * bvec[:, 1]*2.,
     -bval * bvec[:, 0] * bvec[:, 2]*2.,
     -bval * bvec[:, 1] * bvec[:, 1],
     -bval * bvec[:, 1] * bvec[:, 2]*2.,
     -bval * bvec[:, 2] * bvec[:, 2]
     ]).T


def sph2cart(th, ph, units='radians'):
    """
    Convert spherical angles theta/phi to cartesian

    Args:
        th (float or array) : polar angles
        ph (float or array): azimuthal angles
        units (str) : either 'radians' or 'degrees'

    Returns:
        array (Nx3)
    """
    assert units in ['degrees','radians'], "Units must be either 'degrees' or 'radians'"

    if units == 'degrees':
        factor = np.pi/180.
        return np.array([
            np.sin(factor*th) * np.cos(factor*ph),
            np.sin(factor*th) * np.sin(factor*ph),
            np.cos(factor*th)]).T
    else:
        return np.array([
            np.sin(th) * np.cos(ph),
            np.sin(th) * np.sin(ph),
            np.cos(th)]).T
