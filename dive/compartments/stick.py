from .base import Parameter, BaseCompartment
import numpy as np


class Stick(BaseCompartment):
    """
    Represents water with gaussian diffusion only in one direction.
    stick orientation is assumed to be in z direction ([0, 0, 1])
    """
    name = 'stick'
    required_acquisition = ('bval', 'bvec')
    parameters = (
        Parameter("diffusivity", 0., 3.1, 1.7, 'um^2/ms'),
        Parameter("S0", 0., 2., 1.),
    )

    @staticmethod
    def predict(S0, diffusivity, bval, bvec):
        """
        Predicts signal of water in ball

        Args:
            S0: base signal at b=0
            diffusivity: diffusivity of the water
            bval: b-value of the acquisition
            bvec: direction of the applied gradient array (..., 3)
        """
        return S0 * np.exp(-diffusivity * bval * bvec[..., 2] ** 2)
