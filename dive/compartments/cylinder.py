from dive.compartments.base import Parameter, BaseCompartment
from dive.acquisition import calc_bval
import numpy as np

MINIMUM_RADIUS = 1e-10 # to avoid numerical issues
# Constants:
gamma = 2.6752218744 * 1e8
# gamma = 42.577478518*1e6     # [sec]^-1 * [T]^-1
gamma_ms = gamma * 1e-3  # [ms]^-1 *[T]^-1

class Cylinder(BaseCompartment):
    """
    Represents water with gaussian diffusion restricted inside a cylinder.
    This compartment has the following parameters:
    1. Radius of the cylinder.
    2. Diffusivity inside the cylinder.
    3. Signal at b0.

    The cylinder is oriented in the z-direction, so there is no orientation param

    """
    name = 'cylinder'
    required_acquisition = ('bvec', 'pulse_duration', 'diffusion_time', 'G')
    parameters = (
        Parameter("radius", 0., 10., 1., 'um'),
        Parameter("diffusivity", 0., 3.1, 1.7,  'um^2/ms'),
        Parameter("S0", 0., 2., 1.),
    )

    @staticmethod
    def predict(bvec, pulse_duration, diffusion_time, G, radius, diffusivity, S0):
        """
        Predicts signal of water restricted in  a sphere.
        Args:
            pulse_duration: applied gradient time (in ms)
            diffusion_time: diffusion time (in ms)
            G: gradient strength (in mT/m)
            radius: radius of sphere (in um)
            diffusivity: diffusion coefficient inside the sphere
            S0: base signal at b=0
        """
        radius_clip = np.clip(radius,MINIMUM_RADIUS,None)
        G_T_per_micron = G * 1e-3 * 1e-6  # [T] * [um]^-1
        am_r = am[:, np.newaxis] / radius_clip
        GPDsum = compute_GPDsum(am_r, pulse_duration, diffusion_time, diffusivity, radius_clip)

        c2 = bvec[...,2]**2  # cosine^2 of angle between bvec and cylinder orientation
        s2 = 1-c2          # sine^2
        #b  = (gamma_ms*pulse_duration*G_T_per_micron)**2*(diffusion_time-pulse_duration)/3. # bvalue
        b  = calc_bval(G,pulse_duration,diffusion_time)
        log_att_perp = -2. * gamma_ms ** 2 * G_T_per_micron ** 2 * GPDsum * s2
        log_att_para = -b * diffusivity * c2

        return S0 * np.exp(log_att_perp+log_att_para)



# From Camino source
# 60 first roots from the equation j'1(am*x)=0 */
am = np.array([
    1.84118307861360, 5.33144196877749, 8.53631578218074,
    11.7060038949077, 14.8635881488839, 18.0155278304879,
    21.1643671187891, 24.3113254834588, 27.4570501848623,
    30.6019229722078, 33.7461812269726, 36.8899866873805,
    40.0334439409610, 43.1766274212415, 46.3195966792621,
    49.4623908440429, 52.6050411092602, 55.7475709551533,
    58.8900018651876, 62.0323477967829, 65.1746202084584,
    68.3168306640438, 71.4589869258787, 74.6010956133729,
    77.7431620631416, 80.8851921057280, 84.0271895462953,
    87.1691575709855, 90.3110993488875, 93.4530179063458,
    96.5949155953313, 99.7367932203820, 102.878653768715,
    106.020498619541, 109.162329055405, 112.304145672561,
    115.445950418834, 118.587744574512, 121.729527118091,
    124.871300497614, 128.013065217171, 131.154821965250,
    134.296570328107, 137.438311926144, 140.580047659913,
    143.721775748727, 146.863498476739, 150.005215971725,
    153.146928691331, 156.288635801966, 159.430338769213,
    162.572038308643, 165.713732347338, 168.855423073845,
    171.997111729391, 175.138794734935, 178.280475036977,
    181.422152668422, 184.563828222242, 187.705499575101])


def compute_GPDsum(am_r, pulse_duration, diffusion_time, diffusivity, radius):
    dam = diffusivity * am_r * am_r
    e11 = - dam * pulse_duration
    e2  = - dam * diffusion_time
    # dif = diffusion_time - pulse_duration
    e3 = e2-e11 #- np.outer(dam, dif)
    # plus = diffusion_time + pulse_duration
    e4 = e2+e11 #- np.outer(dam, plus)

    nom = -2 * e11 - 2 + \
          (2 * np.exp(e11)) + \
          (2 * np.exp(e2)) - np.exp(e3) - np.exp(e4)
    denom = dam ** 2 * am_r ** 2 * (radius ** 2 * am_r ** 2 - 1)
    return np.sum(nom / denom, axis=0)

