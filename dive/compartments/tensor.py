from .base import Parameter, BaseCompartment
import numpy as np


class Tensor(BaseCompartment):
    """
    Represents water with gaussian diffusion in 3D.
    The tensor is assumed to be aligned along the z-axis ([0,0,1]).

    """
    name = 'tensor'
    required_acquisition = ('bval', 'bvec')
    parameters = (
        Parameter("diffusivity_ax", 0., 3.1, 1.7, 'um^2/ms'),
        Parameter("diffusivity_perp1", 0., 3.1, 1., 'um^2/ms'),
        Parameter("diffusivity_perp2", 0., 3.1, 0.5, 'um^2/ms'),
        Parameter("S0", 0., 2., 1.),
    )

    @staticmethod
    def predict(S0,
                diffusivity_ax,
                diffusivity_perp1,
                diffusivity_perp2,
                bval,
                bvec):
        """
        Predicts signal of water in a tensor

        Args:
            S0: base signal at b=0
            diffusivity_ax: diffusivity of the water parallel to the fibre direction (z-axis)
            diffusivity_perp1: diffusivity of the water orthogonal to the fibre direction (y-axis)
            diffusivity_perp2: diffusivity of the water along the other orthogonal (x-axis)
            bval: b-value of the acquisition
            bvec: direction of the applied gradient array. (..., 3)

            Here, we assume the axial diffusivity to be along the z-axis
        """
        bv2 = bvec * bvec
        decay = - bval * np.sum([bv2[..., idx] * param for idx, param in enumerate([diffusivity_perp2, diffusivity_perp1, diffusivity_ax])], 0)

        return S0 * np.exp(decay)
