from .base import Parameter, BaseCompartment
import numpy as np


class Sphere(BaseCompartment):
    """
    Represents water with gaussian diffusion restricted inside a sphere.
    This compartment has the following parameters:
    1. Radius of the sphere.
    2. Diffusivity inside the sphere.
    3. Signal at b0.


    GPD approximation is used.
    Ref. G.J. Stanisz, A. Szafer, G.A. Wright, R.M. Henkelman
            An analytical model of restricted diffusion in bovine optic nerve
    """
    name = 'sphere'
    required_acquisition = ('pulse_duration', 'diffusion_time', 'G')
    parameters = (
        Parameter("radius", 0., 30., 1., 'um'),
        Parameter("diffusivity", 0., 3.1, 1.7, 'um^2/ms'),
        Parameter("S0", 0., 2., 1.),
    )

    @staticmethod
    def predict(pulse_duration, diffusion_time, G, radius, diffusivity, S0):
        """
        Predicts signal of water restricted in  a sphere.
        Args:
            pulse_duration: applied gradient time (in ms)
            diffusion_time: diffusion time (in ms)
            G: gradient strength (in ?)
            radius: radius of sphere (in um)
            diffusivity: diffusion coefficient inside the sphere
            S0: base signal at b=0
        """
        G_T_per_micron = G * 1e-3 * 1e-6  # [T] * [um]^-1
        am_r = am[:, np.newaxis] / radius
        GPDsum = compute_GPDsum(am_r, pulse_duration, diffusion_time, diffusivity, radius)
        log_att = -2. * gamma_ms ** 2 * G_T_per_micron ** 2 * GPDsum
        return S0 * np.exp(log_att)


# Constants:
gamma = 2.6752218744 * 1e8
# gamma = 42.577478518*1e6     # [sec]^-1 * [T]^-1
gamma_ms = gamma * 1e-3  # [ms]^-1 *[T]^-1

# From Camino source
#  60 first roots from the equation (am*x)j3/2'(am*x)- 1/2 J3/2(am*x)=0
am = np.array([2.08157597781810, 5.94036999057271, 9.20584014293667,
               12.4044450219020, 15.5792364103872, 18.7426455847748,
               21.8996964794928, 25.0528252809930, 28.2033610039524,
               31.3520917265645, 34.4995149213670, 37.6459603230864,
               40.7916552312719, 43.9367614714198, 47.0813974121542,
               50.2256516491831, 53.3695918204908, 56.5132704621986,
               59.6567290035279, 62.8000005565198, 65.9431119046553,
               69.0860849466452, 72.2289377620154, 75.3716854092873,
               78.5143405319308, 81.6569138240367, 84.7994143922025,
               87.9418500396598, 91.0842274914688, 94.2265525745684,
               97.3688303629010, 100.511065295271, 103.653261271734,
               106.795421732944, 109.937549725876, 113.079647958579,
               116.221718846033, 116.221718846033, 119.363764548757,
               122.505787005472, 125.647787960854, 128.789768989223,
               131.931731514843, 135.073676829384, 138.215606107009,
               141.357520417437, 144.499420737305, 147.641307960079,
               150.783182904724, 153.925046323312, 157.066898907715,
               166.492397790874, 169.634212946261, 172.776020008465,
               175.917819411203, 179.059611557741, 182.201396823524,
               185.343175558534, 188.484948089409, 191.626714721361])


def compute_GPDsum(am_r, pulse_duration, diffusion_time, diffusivity, radius):
    dam = diffusivity * am_r * am_r
    e11 = -dam * pulse_duration
    e2 = -dam * diffusion_time
    dif = diffusion_time - pulse_duration
    e3 = -dam * dif
    plus = diffusion_time + pulse_duration
    e4 = -dam * plus
    nom = 2 * dam * pulse_duration - 2 + (2 * np.exp(e11)) + (2 * np.exp(e2)) - np.exp(e3) - np.exp(e4)
    denom = dam ** 2 * am_r ** 2 * (radius ** 2 * am_r ** 2 - 2)
    return np.sum(nom / denom, 0)
