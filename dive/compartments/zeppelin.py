from .base import Parameter, BaseCompartment
import numpy as np


class Zeppelin(BaseCompartment):
    """
    Represents diffusion in an axially symmetric diffusion tensor.
    S = S0 exp(-b*dax*(bvec'*mu).^2)*exp(-b*drad(1-(bvec'*mu).^2))
    stick orientation is assumed to be in z direction (mu=[0, 0, 1])
    """
    name = 'zeppelin'
    required_acquisition = ('bval', 'bvec')
    parameters = (
        Parameter("axial_diffusivity", 0., 3.1, 1.7, 'um^2/ms'),
        Parameter("radial_diffusivity", 0., 3.1, 1., 'um^2/ms'),
        Parameter("S0", 0., 2., 1.),
    )

    @staticmethod
    def predict(S0, axial_diffusivity, radial_diffusivity, bval, bvec):
        """
        Predicts signal of water in zeppelin

        Args:
            S0: base signal at b=0
            axial_diffusivity: diffusivity of the water along the primary eigenvector (fibre orientation)
            radial_diffusivity: diffusivity of the water in the orthogonal plane
            bval: b-value of the acquisition
            bvec: direction of the applied gradient array (..., 3)
        """
        return S0 * np.exp(-axial_diffusivity * bval * bvec[..., 2] ** 2) * \
               np.exp(-radial_diffusivity * bval * (1 - bvec[..., 2] ** 2))
