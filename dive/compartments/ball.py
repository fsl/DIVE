from .base import Parameter, BaseCompartment
import numpy as np


class Ball(BaseCompartment):
    """
    Represents water with isotropic, Gaussian diffusion.
    """
    name = 'ball'
    required_acquisition = ('bval', )
    parameters = (
        Parameter("diffusivity", 0., 4., 3.,'um^2/ms'),
        Parameter("S0", 0., 2., 1.),
    )

    @staticmethod
    def predict(S0, diffusivity, bval):
        """
        Predicts signal of water in ball

        Args:
            S0: base signal at b=0
            diffusivity: diffusivity of the water
            bval: b-value of the acquisition
        """
        return S0 * np.exp(-diffusivity * bval)
