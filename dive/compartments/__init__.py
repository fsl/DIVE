from .ball import Ball
from .stick import Stick
from .zeppelin import Zeppelin
from .sphere import Sphere
from .base import BaseCompartment
from .dot import Dot
from .tensor import Tensor
from .cylinder import Cylinder
from typing import List


def list_compartments() -> List[BaseCompartment]:
    """
    Returns list of available compartments
    """
    return [Stick, Ball, Zeppelin, Tensor, Sphere, Cylinder, Dot]

