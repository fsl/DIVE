from .base import Parameter, BaseCompartment
import numpy as np


class Dot(BaseCompartment):
    """

    Non-diffusing water is represented as a dot model assuming radius of 0
    S = S0

    """
    name = 'dot'
    required_acquisition = ()
    parameters = (
        Parameter("S0", 0., 2., 1.),
    )

    @staticmethod
    def predict(S0):
        """
        Returns signal of water in dot

        Args:
            S0: base signal at b=0
        """
        return S0
