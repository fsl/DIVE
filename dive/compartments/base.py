"""Contains classes describing individual water compartments within the tissue
"""

from abc import ABC, abstractmethod
import numpy as np
from typing import Collection, Union
from dataclasses import dataclass
from typing import List, Union
from ..utils import fibonacci_sphere


@dataclass
class Parameter:
    """
    Represents a single parameter

    Returns:
        [type]: [description]
    """
    name: str
    min_value: Union[int, float]
    max_value: Union[int, float]
    default_value: Union[None, int, float]
    unit: str = ''

    def __post_init__(self):
        self.title = self.name + f' ({self.unit})' if len(self.unit) else self.name

    def bokeh_slider(self, ):
        from bokeh.models import Slider
        return Slider(title=self.title, start=self.min_value, end=self.max_value, value=self.default_value, step=0.01)


class BaseCompartment(ABC):
    """Abstract base class for all compartments
    """
    def __init__(self, **kwargs):
        """
        Creates a new compartment based on the provided Parameter values

        Any parameters not set in `kwargs` will be set to their default value.

        Any non-parameter keys in `kwargs` will lead to a ValueError being raised.
        """
        unrecognised_keys = {key for key in kwargs if key not in self.parameter_names}
        if len(unrecognised_keys) > 0:
            raise ValueError(f"Trying to set unrecognised parameter values for {self.name} compartment: {unrecognised_keys}")
        self.values = {}
        for param in self.parameters:
            self.values[param.name] = kwargs.get(param.name, param.default_value)

    name = ''

    @property
    @abstractmethod
    def required_acquisition(self, ) -> Collection[str]:
        """Acquisition parameters required to evaluate the signal"""
        raise NotImplementedError()

    @property
    @abstractmethod
    def parameters(self, ) -> Collection[Parameter]:
        """Acquisition parameters required to evaluate the signal"""
        raise NotImplementedError()

    @abstractmethod
    def predict(**parameters) -> np.ndarray:
        """Predicts signal based on user-provided parameters and acquisition parameters
        """
        raise NotImplementedError()

    @property
    def parameter_names(self, ):
        """All the parameter names.
        """
        return [p.name for p in self.parameters]

    @property
    def parameter_titles(self,):
        """ All parameter names with their units.
        """
        return [p.title for p in self.parameters]

    @property
    def isotropic(self, ):
        return 'bvec' not in self.required_acquisition

    def combine_params(self, **parameters):
        """
        Returns a combined set of parameters.

        The provided `parameters` should contain all required parameters and optionally any microstructural parameters, 
        which will override the value given on the slider.
        This method will only return parameters that are actually required by the model.
        """
        combined_params = dict(self.values)
        combined_params.update(parameters)

        all_param_names = list(self.required_acquisition) + self.parameter_names
        return {
            name: value for name, value in combined_params.items() 
            if name in all_param_names
        }

    def __call__(self, **parameters) -> np.ndarray:
        return self.predict(**self.combine_params(**parameters))

    def __repr__(self, ):
        arguments = ', '.join(f"{key}={value}" for key, value in self.values.items())
        return f"{self.name}({arguments})"

    def spherical_mean(self, **parameters):
        """
        Computes the spherical mean signal for the given set of parameters
        """
        if self.isotropic:
            return self(**parameters)

        new_params = dict(parameters)
        shape = np.broadcast_shapes(*[np.asarray(arr).shape for key, arr in parameters.items() if key != 'bvec'])
        if len(shape) == 0:
            new_params['bvec'] = fibonacci_sphere(100)
            return np.mean(self(**new_params))
        else:
            arr = np.zeros(shape)
            for bvec in fibonacci_sphere(samples=100):
                parameters['bvec'] = bvec[None, :]
                arr += self(**parameters)
            return arr / 100