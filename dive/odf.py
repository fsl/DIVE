"""Convolve compartment signals with some fibre ODF.

All compartments should point in z-direction for this to work.
"""
import numpy as np
from .utils import fibonacci_sphere


def convolve_crossing(compartment, acquisition, crossing):
    """
    Sums the signal produced from multiple fibre orientation distributions.

    Args:
        compartment: name of response function to convolve with
        acquisition: acquisiton parameters to be input into compartment
        crossing: contains the volume fraction (float), fibre response function (string),
         and the parameters for odf (e.g. orientation in case of sticks, and (odi, orientation) in case of watson)

    """
    # if not dependence on bvecs, return response function/compartment
    # TODO: the case of tensor needs to be fixed.
    if compartment.name in ["ball", "sphere", "tensor","dot"]:
        return compartment(**acquisition)

    signal = 0

    for volume_fraction, dtype, param in crossing:
        func = {'delta': convolve_delta, 'watson': convolve_watson}[dtype]
        new_signal = volume_fraction * func(compartment, acquisition, *param)
        signal = signal + new_signal
    return signal


def convolve_delta(compartment, acquisition, theta, phi):
    """
    Represents the fibre population as a single delta function in a single orientation.
    Convolves this fibre orientation with a response function (i.e. compartment)

    Args:
        compartment: name of response function to convolve with
        acquisition: acquisiton parameters to be input into compartment
        theta: angle with z-axis
        phi: angle in x-y-plane
    """

    # Rotate the gradient direction so that fibre orientation is along the z-axis relative to the gradient direction
    delta_orientation = spherical2cart(theta=theta, phi=phi)
    dotp = np.sum(delta_orientation * acquisition['bvec'], -1)
    new_bvecs = np.stack([np.sqrt(1 - dotp ** 2), np.zeros_like(dotp), dotp], -1)
    new_acquisition = dict(acquisition)  # re-intiailize so you don't change acquisition
    new_acquisition["bvec"] = new_bvecs  # re-initialize the bvecs
    return compartment(**new_acquisition)


def convolve_watson(compartment, acquisition, odi, theta, phi, integral_samples=1000, vectorize=True):
    """
    Convolves the signal from a compartment with a watson distribution given by the
    Orientation dispersion index (between zero and one) and main orientation of the fibers
    
    Args:
        compartment: an instance of compartment class.
        acquisition:
        odi: scalar in range (0, 1)
        theta: angle with z-axis
        phi: angle in x-y-plane
        integral_samples: number of samples for integration

    Returns:
        convolved signal.
    """
    if compartment.isotropic:
        return compartment(**acquisition)

    assert 0 <= odi <= 1, 'ODI must be in range [0,1]'
    if odi == 0:
        return convolve_delta(compartment, acquisition, theta, phi)
    bvec = acquisition['bvec']
    acq = dict(acquisition)
    if bvec.ndim == 1:
        bvec = bvec[np.newaxis, :]

    k = 1 / np.tan(odi * np.pi / 2)
    orientation_cart = spherical2cart(theta=theta, phi=phi)

    samples = fibonacci_sphere(integral_samples)
    wat_pdf = np.exp(k * samples.dot(orientation_cart) ** 2)
    wat_pdf = wat_pdf / wat_pdf.sum()

    if vectorize:
        Norig = acq['bvec'].shape[0]
        for k in acq.keys():
            if (not np.isscalar(acq[k])) and acq[k].shape[0] == Norig:
                acq[k] = np.concatenate([acq[k]] * integral_samples)
        dotp = (bvec.dot(samples.T)).T.ravel()
        acq['bvec'] = np.stack([np.sqrt(np.maximum(1 - dotp ** 2, 0)), np.zeros_like(dotp), dotp], axis=-1)
        resp = compartment(**acq).reshape((integral_samples, -1))
        return wat_pdf.dot(resp)
    else:
        resp = np.zeros((integral_samples, bvec.shape[0]))
        for n_i, n in enumerate(samples):
            dotp = bvec.dot(n)
            acq['bvec'] = np.stack([np.sqrt(1 - dotp ** 2), np.zeros_like(dotp), dotp], axis=-1)
            resp[n_i] = compartment(**acq)

        sig = wat_pdf.dot(resp)
        return sig


def spherical2cart(theta, phi, r=1):
    """
    Converts spherical to cartesian coordinates
    :param theta: angel from z axis
    :param phi: angle from x axis
    :param r: radius
    :return: tuple [x, y, z]-coordinates
    """
    z = r * np.cos(theta)
    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    return x, y, z


def cart2spherical(n):
    """
    Converts to spherical coordinates
    :param n: (:, 3) array containing vectors in (x,y,z) coordinates
    :return: tuple with (phi, theta, r)-coordinates
    """
    n = np.atleast_2d(n)
    r = np.sqrt(np.sum(n ** 2, axis=1))
    theta = np.arccos(n[:, 2] / r)
    phi = np.arctan2(n[:, 1], n[:, 0])
    phi[r == 0] = 0
    theta[r == 0] = 0
    return theta, phi, r
